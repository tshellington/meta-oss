DESCRIPTION = "A nice Qt wrapper around the connman api"
LICENSE = "LGPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/LGPL-3.0;md5=bfccfe952269fff2b407dd11f2f3083b"

inherit qmake2

QMAKE_PROFILES = "qconnman.pro"

PR = "r3.${SRCPV}"

SRCREV = "${AUTOREV}"

SRC_URI = " \
            git://bitbucket.org/devonit/qconnman.git;protocol=https \
          "

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}


