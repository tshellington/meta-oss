DESCRIPTION = "A network authentication protocol"
HOMEPAGE = "http://web.mit.edu/Kerberos/"
SECTION = "console/network"
PR = "r1"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}/doc/copyright.texinfo;md5=f80249ba8f7a4d1b95131ebe54d112a9"

DEPENDS = "perl-native ncurses util-linux e2fsprogs-native openssl openldap e2fsprogs"
PARALLEL_MAKE = ""

inherit autotools binconfig gettext

SRC_URI = "http://web.mit.edu/kerberos/dist/krb5/1.10/krb5-${PV}-signed.tar"
#           file://fix-uclibc-ruserpass-collision.patch;apply=no \
#           file://copyperms.patch;apply=no"

S = "${WORKDIR}/${PN}-${PV}/src/"

# Will clean this up...
EXTRA_OECONF += "--without-tcl krb5_cv_attr_constructor_destructor=yes ac_cv_func_regcomp=yes \
                  ac_cv_printf_positional=yes ac_cv_file__etc_environment=yes \
                  ac_cv_file__etc_TIMEZONE=no --with-system-et --enable-dns-for-realm --with-ldap \
                  --enable-kdc-replay-cache --with-system-et-libs --with-system-et-includes"
CFLAGS_append += "-DDESTRUCTOR_ATTR_WORKS=1 -I${STAGING_INCDIR}/et"
LDFLAGS_append += "-lpthread"

FILES_${PN}-doc += "/usr/share/examples"

krb5_do_unpack() {
	tar xzf ${WORKDIR}/krb5-${PV}.tar.gz -C ${WORKDIR}/
}

python do_unpack() {
	bb.build.exec_func('base_do_unpack', d)
	bb.build.exec_func('krb5_do_unpack', d)
}

do_configure() {
	oe_runconf
}

#do_install_append () {
#	mv ${D}${bindir}/ftp ${D}${bindir}/ftp.${PN}
#	mv ${D}${sbindir}/ftpd ${D}${sbindir}/ftpd.${PN}
#	mv ${D}${bindir}/telnet ${D}${bindir}/telnet.${PN}
#	mv ${D}${sbindir}/telnetd ${D}${sbindir}/telnetd.${PN}
#}

#pkg_postinst_${PN} () {
##!/bin/sh
#	update-alternatives --install ${bindir}/ftp ftp ftp.${PN} 100
#	update-alternatives --install ${sbindir}/ftpd ftpd ftpd.${PN} 100
#	update-alternatives --install ${bindir}/telnet telnet telnet.${PN} 100
#	update-alternatives --install ${sbindir}/telnetd telnetd telnetd.${PN} 100
#}

#pkg_prerm_${PN} () {
##!/bin/sh
#	update-alternatives --remove ftp ftp.${PN} 100
#	update-alternatives --remove ftpd ftpd.${PN} 100
#	update-alternatives --remove telnet telnet.${PN} 100
#	update-alternatives --remove telnetd telnetd.${PN} 100
#}

SRC_URI[md5sum] = "ddacb6ad7399681ad1506f435a2683b6"
SRC_URI[sha256sum] = "52c719de1a7f042109486a80e7b69e43555186a2acf26c1e5ab83a3ab1cebe3d"
