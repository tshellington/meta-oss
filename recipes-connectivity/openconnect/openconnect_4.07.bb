DESCRIPTION = "OpenConnect is a client for Cisco's AnyConnect SSL VPN, which is supported by the ASA5500 Series, by IOS 12.4(9)T or later on Cisco SR500, 870, 880, 1800, 2800, 3800, 7200 Series and Cisco 7301 Routers, and probably others."
HOMEPAGE = "http://www.infradead.org/openconnect/index.html"
LICENSE = "LGPL21"
LIC_FILES_CHKSUM = "file://COPYING.LGPL;md5=243b725d71bb5df4a1e5920b344b86ad"

DEPENDS += "openssl gnutls vpnc"

EXTRA_OECONF += "--with-vpnc-script=/etc/vpnc/vpnc-script"

SRC_URI = "ftp://ftp.infradead.org/pub/openconnect/${PN}-${PV}.tar.gz"
SRC_URI[md5sum] = "61f26e7936d8b26c0f7e8119b7ef84b2"
SRC_URI[sha256sum] = "14cacb17813714ed16c58dbcb33cd77463c4e3c4e52e955e8b59a43833d3b6f3"

inherit autotools pkgconfig

