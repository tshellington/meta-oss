require recipes-connectivity/connman/connman.inc

SRC_URI  = " \
            http://www.kernel.org/pub/linux/network/connman/connman-${PV}.tar.bz2 \
            file://add_xuser_dbus_permission.patch \
            file://0001-plugin.h-Change-visibility-to-default-for-debug-symb.patch \
            file://0001-dhcp-don-t-set-hostname.patch \
           "
PR = "r1"
SRC_URI[md5sum] = "8196824f400719dd172222a355ad9592"
SRC_URI[sha256sum] = "a7f90994a60708aab8ac3d343194bbd97df229d3cd2c25559d754e2edad21255"
