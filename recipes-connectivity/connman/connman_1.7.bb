require recipes-connectivity/connman/connman.inc

SRC_URI  = " \
            http://www.kernel.org/pub/linux/network/connman/connman-1.7.tar.bz2 \
            file://add_xuser_dbus_permission.patch \
            file://0001-plugin.h-Change-visibility-to-default-for-debug-symb.patch \
            file://0001-dhcp-don-t-set-hostname.patch \
           "
PR = "r11"
SRC_URI[md5sum] = "9c6cf5a95989ae72d90b270ac7c6e38c"
SRC_URI[sha256sum] = "d330cebc80be05aac924c973060f656de58427e57049e879495c186882b1ac5a"

