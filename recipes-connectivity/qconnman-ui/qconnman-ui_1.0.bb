# require recipes-connectivity/connman/connman.inc

LICENSE  = "LGPLv2.1"

LIC_FILES_CHKSUM = "file://README.md;md5=b4ab15c73a003bc7925de60ed70f1dd4"

DEPENDS = "libqconnman" 
inherit qmake2 qt4x11

S = "${WORKDIR}/git"

SRCREV = "master"

SRC_URI  = " \
            git://github.com/OSSystems/qconnman-ui.git;protocol=git \
           "
           
PR = "r1.1"
SRC_URI[md5sum] = "8196824f400719dd172222a355ad9592"
SRC_URI[sha256sum] = "a7f90994a60708aab8ac3d343194bbd97df229d3cd2c25559d754e2edad21255"
