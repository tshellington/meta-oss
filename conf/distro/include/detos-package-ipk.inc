INHERIT += "package_ipk"

DEVONIT_FEED_CONFIGS ?= "devonit-feed-configs"

EXTRAOPKGCONFIG = "opkg-config-base ${DEVONIT_FEED_CONFIGS}"

DEVONIT_IPK_UPDATE_ALTERNATIVES ?= "update-alternatives-cworth"

DISTRO_UPDATE_ALTERNATIVES = "${DEVONIT_IPK_UPDATE_ALTERNATIVES}"

# we need the same provider for opkg and u-a
PREFERRED_PROVIDER_virtual/update-alternatives-native ?= "opkg-native"
PREFERRED_PROVIDER_virtual/update-alternatives = "${DEVONIT_IPK_UPDATE_ALTERNATIVES}"
VIRTUAL-RUNTIME_update-alternatives = "${DEVONIT_IPK_UPDATE_ALTERNATIVES}"

