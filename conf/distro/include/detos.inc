# Preserve original DISTRO value
USERDISTRO := "${DISTRO}"
DISTRO = "detos"

BUILDNAME = "${DISTRO_VENDOR} ${DISTRO_VERSION}"

TARGET_VENDOR = "-devonit"
SDK_VENDOR = "-devonitsdk"

SOC_FAMILY ?= ""
IMAGE_CLASSES = "image_types image_types_uboot"

DEVON_MIRRORS ?= "http://downloads.devonit.com/Development/accounts/development/sources/"

#Set the right arch for the feeds
#Alphabetically sorted

FEED_ARCH ?= "${TUNE_PKGARCH}"

# Add FEED_ARCH to machine overrides so we get access to e.g. 'armv7a' and 'sh4'
# Hopefully we'll never see a machine or arch with 'all' as substring
MACHINEOVERRIDES .= ":${@bb.data.getVar('FEED_ARCH', d,1).replace('all','noarch')}"

# Put links to sources in deploy/sources to make it easier for people to be GPL compliant
#INHERIT += "src_distribute_local"
#SRC_DIST_LOCAL ?= "symlink"

SYSTEMDDISTRO = "angstrom"

# Can be "glibc", "eglibc" or "uclibc"
TCLIBC ?= "eglibc"

# The things *libc can provide.
PREFERRED_PROVIDER_virtual/libc ?= "${TCLIBC}"
PREFERRED_PROVIDER_virtual/${TARGET_PREFIX}libc-initial ?= "${TCLIBC}-initial"
PREFERRED_PROVIDER_virtual/${TARGET_PREFIX}libc-for-gcc ?= "${TCLIBC}"
PREFERRED_PROVIDER_virtual/libiconv ?= "${TCLIBC}"

# And the same as above for the nativesdk
PREFERRED_PROVIDER_virtual/libc-nativesdk ?= "${TCLIBC}-nativesdk"
PREFERRED_PROVIDER_virtual/${SDK_PREFIX}libc-initial-nativesdk ?= "${TCLIBC}-initial-nativesdk"
PREFERRED_PROVIDER_virtual/${SDK_PREFIX}libc-for-gcc-nativesdk ?= "${TCLIBC}-nativesdk"

PREFERRED_PROVIDER_virtual/gettext ??= "gettext"
PREFERRED_PROVIDER_linux-libc-headers-nativesdk ?= "linux-libc-headers-nativesdk"

#require conf/distro/include/angstrom-${TCLIBC}.inc

# Set some java bits
require conf/distro/include/angstrom-java.inc
require conf/distro/include/angstrom-jalimo.conf

ARM_INSTRUCTION_SET ?= "arm"
# "arm" "thumb"
#    The instruction set the compiler should use when generating application
#    code.  The kernel is always compiled with arm code at present.  arm code
#    is the original 32 bit ARM instruction set, thumb code is the 16 bit
#    encoded RISC sub-set.  Thumb code is smaller (maybe 70% of the ARM size)
#    but requires more instructions (140% for 70% smaller code) so may be
#    slower.

#use debian style naming
INHERIT += "debian"

#activate config checker
INHERIT += "sanity" 

#activate the blacklister
#INHERIT += "blacklist"

#make devshell available as task
INHERIT += "devshell" 

#run QA tests on builds and packages and log them  
INHERIT += "insane"
QA_LOG = "1"

#run QA tests on recipes
#INHERIT += "recipe_sanity"

#have a shared state a.k.a package-staging2
INHERIT += "sstate"

DETOS_PKG_FORMAT ?= "ipk"
require conf/distro/include/detos-package-${DETOS_PKG_FORMAT}.inc

# We don't want to keep OABI compat
ARM_KEEP_OABI = "0"

#Generate locales on the buildsystem instead of on the target. Speeds up first boot, set to "1" to enable
PREFERRED_PROVIDER_qemu-native = "qemu-native"
ENABLE_BINARY_LOCALE_GENERATION ?= "1"

# We only want to build UTF8 locales
LOCALE_UTF8_ONLY = "0"

# Prelink images
INHERIT += "image-prelink"

#Name the generated images in a sane way
IMAGE_NAME = "${DISTRO_NAME}-${DISTRO_VERSION}.${DISTRO_PR}-${MACHINE}-${IMAGE_BASENAME}-${DATE}"
DEPLOY_DIR_IMAGE = "${DEPLOY_DIR}/images/${MACHINE}"

# Angstrom *always* has some form of release config, so error out if someone thinks he knows better 
DISTRO_CHECK := "${@bb.data.getVar("DISTRO_VERSION",d,1) or bb.fatal('Remove this line or set a dummy DISTRO_VERSION if you really want to build an unversioned distro')}"

# We want images supporting the following features (for task(-core)-base)
DISTRO_FEATURES += "alsa argp bluetooth ext2 ipv4 ipv6 irda largefile nfs pam pci pcmcia ppp smbfs usbgadget usbhost vfat wifi xattr zeroconf"

# USE-flag like features
DISTRO_FEATURES += "tk"
DISTRO_FEATURES += "x11"
DISTRO_FEATURES += "3g"
DISTRO_FEATURES += "pulseaudio"

# Inherit the default LIBC features superset from OE-core
DISTRO_FEATURES += "${DISTRO_FEATURES_LIBC}"

#############################################################################
# FEATURE SELECTION
#############################################################################
IMAGE_LINGUAS ?= " \
                   en-us \
                   en-gb \
                   de-de \
                   es-es \
                   es-mx \
                   fr-fr \
                   fr-ca \
                   ja-jp \
                   ko-kr \
                   pt-br \
                   zh-cn \
                   zh-tw \
                 "

GLIBC_GENERATE_LOCALES ?= "all"

DEFAULT_TIMEZONE ?= "US/Central"
DEFAULT_LANG ?= "en_US.UTF-8"

DISTRO_XORG_CONFIG_MANAGER = "udev"

USER_UID = "1000"
USER_GID = "1000"
USER_HOME = "/home/${USER_NAME}"
USER_NAME ?= "detos"


TERMCMD = "${XTERM_TERMCMD}"
TERMCMDRUN = "${XTERM_TERMCMDRUN}"
SERIAL_CONSOLE = '${@base_conditional("DISTRO_TYPE", "release", "", "115200 ${SERIAL_PORT}",d)}'
USE_VT = '${@base_conditional("DISTRO_TYPE", "release", "", "1",d)}'
SYSVINIT_ENABLED_GETTYS = '${@base_conditional("DISTRO_TYPE", "release", "", "1",d)}'

DISTRO_EXTRA_APPS_RELEASE ?= ""
DISTRO_EXTRA_APPS_DEBUG ?= "task-detos-debug"
DISTRO_SSH_DAEMON ?= "dropbear"
DISTRO_BLUETOOTH_MANAGER ?= "bluez4"
DISTRO_FEATURES += "acpi"
DISTRO_FEATURES += "eabi"
DISTRO_FEATURES += "mplt"
DISTRO_FEATURES += ' ${@["", "thumb-interwork"][bb.data.getVar('THUMB_INTERWORK', d, 1) == "yes"]}'
DISTRO_FEATURES += "opengl"
DISTRO_FEATURES += "x11"

# use debian libname style
INHERIT += "debian"
# use our cached mirrors
INHERIT += "devonit-mirrors"
#run QA tests on builds and packages and log them  
INHERIT += "insane"
QA_LOG = "1"
#run QA tests on recipes
INHERIT += "recipe_sanity"
#make devshell available as task
INHERIT += "devshell"
#activate config checker
INHERIT += "sanity" 

#use ipk as the format
PREFERRED_PKG_FORMAT = "ipk"

#default to tarball
IMAGE_FSTYPES ?= "tar.gz"

DEBUG_STRING = "${@base_conditional('DISTRO_TYPE', 'release', '', '-debug',d)}"
#DISTRO_PR .= "${DEBUG_STRING}"
BUILDNAME = "${DISTRO_BRAND} ${DISTRO_VERSION}.${DISTRO_PR} ${DATE}"
CACHE = "${TMPDIR}/cache/${TCLIBC}/${MACHINE}"
DEPLOY_DIR = "${TMPDIR}/deploy/${TCLIBC}"
DEPLOY_DIR_IMAGE = "${DEPLOY_DIR}/images/${MACHINE}"

SPLASH = "${@bb.data.getVar('PREFERRED_PROVIDER_virtual/psplash',d, '')}"
BRANDING = "${@bb.data.getVar('PREFERRED_PROVIDER_virtual/branding',d, '')}"

KERNEL = "kernel26"
MACHINE_KERNEL_VERSION = "2.6"
SOC_FAMILY ?= ""
MACHINE_CLASS ?= ""
MACHINE_OVERRIDES += " ${FEED_ARCH} ${SOC_FAMILY} ${MACHINE_CLASS} "

# Ship extra debug utils in the rootfs when doing a debug build
DISTRO_EXTRA_RDEPENDS += '${@base_conditional("DISTRO_TYPE", "release", "${DISTRO_EXTRA_APPS_RELEASE}", "${DISTRO_EXTRA_APPS_DEBUG}",d)}'

# Additional content I (only valid if you include task-base)
# util-linux-ng-mount util-linux-ng-umount: busybox mount is broken
# angstrom-libc-fixup-hack: fixes an obscure bug with libc.so symlink
DISTRO_EXTRA_RDEPENDS += "\
                          util-linux-mount \
                          util-linux-umount \
                          angstrom-libc-fixup-hack \
                         "

# Set the toolchain type (internal, external) and brand (generic, csl etc.)
TOOLCHAIN_TYPE ?= "internal"
TOOLCHAIN_BRAND ?= ""
#uncomment for csl
#fixme: make this depend on TOOLCHAIN_BRAND
#KERNEL_CCSUFFIX_armv7a = "-4.2.1+csl-arm-2007q3-53"

LINUX_LIBC_HEADERS_VERSION ?= "3.2"
PREFERRED_VERSION_linux-libc-headers = "${LINUX_LIBC_HEADERS_VERSION}"
PREFERRED_VERSION_linux-libc-headers-native = "${LINUX_LIBC_HEADERS_VERSION}"
PREFERRED_VERSION_linux-libc-headers-nativesdk = "${LINUX_LIBC_HEADERS_VERSION}"

# Can be "glibc", "eglibc" or "uclibc"
TCLIBC ?= "eglibc"

DETOS_EGLIBC_VERSION              ?= "2.15"
DETOS_GLIBC_VERSION               ?= "2.10.1"
DETOS_GCC_VERSION                 ?= "4.6%"
DETOS_BINUTILS_VERSION            ?= "2.22"
PREFERRED_QT_VERSION              ?= "4.8%"


PREFERRED_VERSION_ica 			           ?= "12.1.0"
PREFERRED_VERSION_glibc                    ?= "${DETOS_GLIBC_VERSION}"
PREFERRED_VERSION_glibc-initial            ?= "${DETOS_GLIBC_VERSION}"
PREFERRED_VERSION_glibc-nativesdk          ?= "${DETOS_GLIBC_VERSION}"
PREFERRED_VERSION_glibc-initial-nativesdk  ?= "${DETOS_GLIBC_VERSION}"
PREFERRED_VERSION_eglibc                   ?= "${DETOS_EGLIBC_VERSION}"
PREFERRED_VERSION_eglibc-initial           ?= "${DETOS_EGLIBC_VERSION}"
PREFERRED_VERSION_eglibc-nativesdk         ?= "${DETOS_EGLIBC_VERSION}"
PREFERRED_VERSION_eglibc-initial-nativesdk ?= "${DETOS_EGLIBC_VERSION}"
PREFERRED_VERSION_eglibc-locale            ?= "${DETOS_EGLIBC_VERSION}"
PREFERRED_VERSION_cross-localedef-native   ?= "${DETOS_EGLIBC_VERSION}"
PREFERRED_VERSION_binutils                 ?= "${DETOS_BINUTILS_VERSION}"
PREFERRED_VERSION_binutils-cross           ?= "${DETOS_BINUTILS_VERSION}"
PREFERRED_VERSION_binutils-crosssdk        ?= "${DETOS_BINUTILS_VERSION}"
PREFERRED_VERSION_binutils-cross-canadian-${TRANSLATED_TARGET_ARCH} ?= "${DETOS_BINUTILS_VERSION}"
PREFERRED_VERSION_gcc-cross-canadian-${TRANSLATED_TARGET_ARCH} ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc                      ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-cross                ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-cross-initial        ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-cross-intermediate   ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-cross-canadian       ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-crosssdk             ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-crosssdk-initial     ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-crosssdk-intermediate ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-runtime              ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_gcc-runtime-nativesdk    ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_libgcc                   ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_libgcc-nativesdk         ?= "${DETOS_GCC_VERSION}"
PREFERRED_VERSION_qt4-tools-native    = "${PREFERRED_QT_VERSION}"
PREFERRED_VERSION_qt4-tools-sdk       = "${PREFERRED_QT_VERSION}"
PREFERRED_VERSION_qt4-tools-nativesdk = "${PREFERRED_QT_VERSION}"
PREFERRED_VERSION_qt4-embedded        = "${PREFERRED_QT_VERSION}"
PREFERRED_VERSION_qt4-embedded-gles   = "${PREFERRED_QT_VERSION}"
PREFERRED_VERSION_qt4-x11-free        = "${PREFERRED_QT_VERSION}"
PREFERRED_VERSION_qt4-x11-free-gles   = "${PREFERRED_QT_VERSION}"
PREFERRED_VERSION_qt4-native          = "${PREFERRED_QT_VERSION}"


# Yocto tweaks
require conf/distro/include/detos-core-tweaks.inc
require conf/distro/include/toolchain-${TOOLCHAIN_TYPE}.inc

## This must be last so we force the settings we want after all of the other includes
require conf/distro/include/detos-xserver.inc
require conf/distro/include/detos-initrd-formats.inc
require conf/distro/include/detos-image-formats.inc
require conf/distro/include/arch-tweaks.inc

PREFERRED_VERSION_grub = "0.97"
PREFERRED_VERSION_grub-native = "0.97"
PREFERRED_VERSION_uim-input = "1.8.1"
PREFERRED_VERSION_openssl = "0.9.8x"

PREFERRED_PROVIDER_ntpdate = 'ntp'

PREFERRED_VERSION_freerdp = "1.0.1"
PREFERRED_VERSION_vmware-view = "1.6.0"
