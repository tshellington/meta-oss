#
# This file exists to allow building of initramfs images using compression
# formats that newer linux kernels can read, but the stable version of
# the openembedded tree that we are using does not yet include
#

# There's a little bit of a disconnect between these compression levels
# and the one's listed in bitbake.conf:
#  * LZOP's doesn't exist
#  * LZMA's isn't defined, but the IMAGE_CMD_cpio.lzma hardcodes a -9 in
#    the command
#  * XZ's is defined to be ' -9 -e ' in bitbake.conf
# We want to prefer values that are set here instead of the one's in
# bitbake.conf
LZOP_COMPRESSION_LEVEL ?= " -9 "
LZMA_COMPRESSION_LEVEL ?= " -9 "
XZ_COMPRESSION_LEVEL   ?= " -9 "

# These depends aren't in bitbake.conf
# cpio.lzma and cpio.xz *are* in bitbake.conf, so we don't define
# them here
IMAGE_DEPENDS_cpio.lzo         = "lzop-native"
IMAGE_DEPENDS_cpio.lzo.u-boot  = "lzop-native u-boot-mkimage-native"
IMAGE_DEPENDS_cpio.lzma.u-boot = "lzma-native u-boot-mkimage-native"
IMAGE_DEPENDS_cpio.xz.u-boot   = "xz-native u-boot-mkimage-native"

# normal and u-boot initrd image gen for LZO compression
IMAGE_CMD_cpio.lzo = "type cpio >/dev/null; cd ${IMAGE_ROOTFS} && (find . | cpio -o -H newc | lzop -c ${LZOP_COMPRESSION_LEVEL} > ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.lzo) ${EXTRA_IMAGECMD}"
IMAGE_CMD_cpio.lzo.u-boot = "type cpio >/dev/null; cd ${IMAGE_ROOTFS} && (find . | cpio -o -H newc | lzop -c ${LZOP_COMPRESSION_LEVEL}  > ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.lzma) ${EXTRA_IMAGECMD}; mkimage -A ${UBOOT_ARCH} -O linux -T ramdisk -C gzip -n ${IMAGE_NAME} -d ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.lzo ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.lzo.u-boot"

# cpio.lzma is already in bitbake.conf
IMAGE_CMD_cpio.lzma.u-boot = "type cpio >/dev/null; cd ${IMAGE_ROOTFS} && (find . | cpio -o -H newc | lzma -c ${LZMA_COMPRESSION_LEVEL}  > ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.lzma) ${EXTRA_IMAGECMD}; mkimage -A ${UBOOT_ARCH} -O linux -T ramdisk -C gzip -n ${IMAGE_NAME} -d ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.lzma ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.lzma.u-boot"

# cpio.xz is already in bitbake.conf
IMAGE_CMD_cpio.xz.u-boot = "type cpio >/dev/null; cd ${IMAGE_ROOTFS} && (find . | cpio -o -H newc | xz -c ${XZ_COMPRESSION_LEVEL} > ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.xz) ${EXTRA_IMAGECMD}; mkimage -A ${UBOOT_ARCH} -O linux -T ramdisk -C gzip -n ${IMAGE_NAME} -d ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.xz ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.cpio.xz.u-boot"

