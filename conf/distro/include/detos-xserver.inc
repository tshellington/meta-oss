XSERVER += "\
           xf86-input-evdev \
           xf86-input-mtev \
           xf86-input-mouse \
           xf86-video-displaylink \
           xf86-video-fbdev \
           xf86-input-keyboard \
           xserver-xorg \
           xserver-xorg-module-xaa \
           xserver-xorg-extension-dri \
           xserver-xorg-extension-dri2 \
           xserver-xorg-extension-glx \
	   xserver-xorg-extension-dbe \
           xserver-xorg-module-libwfb \
           xserver-xorg-extension-extmod \
           xserver-xorg-module-libint10 \
           xserver-xorg-multimedia-modules \
           xserver-common \
          "

