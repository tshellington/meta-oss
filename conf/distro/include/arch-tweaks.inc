#### platform stuff we get from other layers
PREFERRED_PROVIDER_aufs-module_beagleboard = "aufs3-module"
PREFERRED_PROVIDER_aufs-util_beagleboard = "aufs3-util"
PREFERRED_VERSION_aufs3-module_beagleboard = "3.2"
PREFERRED_VERSION_aufs3-util_beaglboard = "3.0"
SERIAL_PORT_beagleboard = "ttyO0"
OCF_KVER_beagleboard = "3.2.1"
ROOTFS_FMT_beagleboard = "squashfs"
INITRAMFS_FMT_beagleboard = "cpio.gz.u-boot"

PREFERRED_PROVIDER_aufs-module_beaglebone = "aufs3-module"
PREFERRED_PROVIDER_aufs-util_beaglebone = "aufs3-util"
PREFERRED_VERSION_aufs3-module_beaglebone = "3.2"
PREFERRED_VERSION_aufs3-util_beaglbone = "3.0"
SERIAL_PORT_beaglebone = "ttyO0"
OCF_KVER_beaglebone = "3.2.1"
ROOTFS_FMT_beaglebone= "squashfs"
INITRAMFS_FMT_beaglebone = "cpio.gz.u-boot"

SERIAL_PORT_cubox = "ttyS0"

SERIAL_PORT_allwinner-a10 = "ttyS0"

