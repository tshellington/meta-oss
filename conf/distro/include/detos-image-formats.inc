# Extra image format metadata

# increase inode/block ratio for ext2 filesystem
EXTRA_IMAGECMD_ext2 = "-i 8192"

IMAGE_DEPENDS_squashfs-lzo = "lzop-native"
IMAGE_DEPENDS_squashfs-xz = "xz-native"

IMAGE_CMD_squashfs-lzo = "mksquashfs ${IMAGE_ROOTFS} ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.squashfs-lzo ${EXTRA_IMAGECMD} -noappend -comp lzo"

IMAGE_CMD_squashfs-xz = "mksquashfs ${IMAGE_ROOTFS} ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.squashfs ${EXTRA_IMAGECMD} -noappend -comp xz -Xbcj arm"
 
devonit_mkext4fs () {
	genext2fs -b $ROOTFS_SIZE -d ${IMAGE_ROOTFS} ${EXTRA_IMAGECMD} $1
	tune2fs -O extents,uninit_bg,dir_index,has_journal $1
	e2fsck -yfDC0 $1 || chk=$?
	case $chk in
	0|1|2)
	    ;;
	*)
	    return $chk
	    ;;
	esac
}

IMAGE_CMD_ext4 () {
	devonit_mkext4fs ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.ext4
}

