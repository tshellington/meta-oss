#@TYPE: Machine
#@NAME: DM814x evm
#@DESCRIPTION: Machine configuration for the DM814x evm
require conf/machine/include/ti814x.inc

#TCMODE = "external-csl"
#TCLIBC = "glibc"

PREFERRED_PROVIDER_virtual/xserver = "xserver-xorg"
XSERVER += "\
	xf86-video-ti81xx \
	"
#GLIBC_INTERNAL_USE_BINARY_LOCALE = "precompiled"
#ENABLE_BINARY_LOCALE_GENERATION = "0"


GUI_MACHINE_CLASS = "bigscreen"

#PREFERRED_PROVIDER_jpeg = "jpeg-turbo"
PREFERRED_PROVIDER_pstree = "pstree"
PREFERRED_PROVIDER_virtual/bootloader = "u-boot"
PREFERRED_PROVIDER_virtual/kernel = "linux-omap3"

PREFERRED_VERSION_u-boot = "2010.06-psp04.04.00.01"

PREFERRED_PROVIDER_aufs-module = "aufs2.2-module"
PREFERRED_PROVIDER_aufs-util = "aufs2-util"
PREFERRED_VERSION_aufs2.2-module = "37"
PREFERRED_VERSION_aufs2-util = "2.2"

AUFS_VERSION = "aufs2.2-37"
AUFS_REV = "d8dde07a7cb60196b4d8df4d6976d379c87cc537"

#try to fix pseudo
NO32LIBS = "0"

PREFERRED_VERSION_libdrm = "2.4.23-ti"
PREFERRED_VERSION_libxv = "1.0.6-ti"
PREFERRED_VERSION_videoproto = "2.3.1-ti"
PREFERRED_VERSION_xf86-video-ti81xx = "0.1.16"
PREFERRED_VERSION_xserver-xorg = "1.10.1"
#PREFERRED_PROVIDER_virtual/${TARGET_PREFIX}libc-initial = "external-csl-toolchain"
PREFERRED_VERSION_ica = "12.1.0"
PREFERRED_VERSION_flash-plugin = "10.1"

PREFERRED_VERSION_gstreamer        = "0.10.32"
PREFERRED_VERSION_gst-plugins-base = "0.10.32"
PREFERRED_VERSION_gst-plugins-good = "0.10.28"
PREFERRED_VERSION_gst-plugins-bad  = "0.10.21"
PREFERRED_VERSION_gst-plugins-ugly = "0.10.16"
#PREFERRED_VERSION_gst-ffmpeg = "0.10.12"

PREFERRED_MESA_VERSION            ?= "7.9.1"
PREFERRED_VERSION_mesa            = "${PREFERRED_MESA_VERSION}"
PREFERRED_VERSION_mesa-dri        = "${PREFERRED_MESA_VERSION}"
PREFERRED_VERSION_mesa-xlib       = "${PREFERRED_MESA_VERSION}"

PREFERRED_PROVIDER_linux-firmware-wl12xx = "ti-linux-firmware-wl12xx"

PSPRELEASE := "04.01.00.06"

require ezsdk_5_03_01_15.inc

# Guesswork
SERIAL_PORT = "ttyO0"

CONSOLE_ARGS = "${@base_conditional("DISTRO_TYPE", "release", "", "console=${SERIAL_PORT},115200n8",d)}"
BOOTARGS = "${CONSOLE_ARGS} rootwait root=/dev/ram rw mem=352M mem=320M@0x9FC00000 earlyprink notifyk.vpssm3_sva=0xBF900000 vmalloc=500M vram=102M printk.time=y ramdisk_size=65536 live-config boot=live quickreboot live-media=/dev/mmcblk0p1 quickusbmodules"

USE_VT = "1"
SYSVINIT_ENABLED_GETTYS = "1"

EXTRA_IMAGEDEPENDS += " u-boot-min-sd "

# MACHINE_EXTRA_RDEPENDS += " \
# 	gst-gemxvimagesink \
# 	gst-plugin-h264 \
# "
MACHINE_EXTRA_DEPENDS += " ti-linuxutils mesa-dri ti-omx osal edma3ldd ti-utils "

#
# This magic is used for us to be able to use CSL's toolchain for the 814x
# build... The only piece of this that should really be customizable is
# the TOOLCHAIN_PATH variable, which can be overridden from the builder's
# local.conf file.
#
#EXTERNAL_TOOLCHAIN   ?= "/opt/arm-2009q1"
#TOOLCHAIN_VENDOR  = "-none"
#TOOLCHAIN_TYPE    = "external"
#TOOLCHAIN_BRAND   = "csl"

# Prefer xz compression for the initramfs - this speeds boot on the SD card.
# only --check=none or --check=crc32 will work with the decompression code
# in the kernel (and xz defaults to crc64). We set it to none, since u-boot
# already computes a crc for the file.
#XZ_COMPRESSION_LEVEL = "--check=none --arm --lzma2=preset=9e"
INITRAMFS_FMT     = "cpio.lzma.u-boot"

DETOS_GCC_VERSION = "4.5"

ROOTFS_FMT = "squashfs"
ROOT_FLASH_SIZE = "230"
IMAGE_ROOTFS_SIZE_ext2 ?= "230000"

MACHINE_KERNEL_PR = "r16"
