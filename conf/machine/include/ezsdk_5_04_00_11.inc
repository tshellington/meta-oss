####
# EZSDK version 5.4.0.11
####

# these three are overridden in the fx1.conf file 
PREFERRED_VERSION_linux-omap3               ?= "2.6.37-ti814xpsp-${PSPRELEASE}"
PREFERRED_VERSION_u-boot                    ?= "2010.06-psp${PSPRELEASE}"
PREFERRED_VERSION_u-boot-min-sd             ?= "2010.06-psp${PSPRELEASE}"

PREFERRED_VERSION_ti-ipc                    ?= "1_23_05_40"
PREFERRED_VERSION_ti-cgt6x                  ?= "7_3_1"
PREFERRED_VERSION_xdctools                  ?= "3_22_04_46"
PREFERRED_VERSION_sysbios                   ?= "6_32_05_54"
PREFERRED_VERSION_syslink                   ?= "2_00_05_85"
PREFERRED_VERSION_ti-linuxutils             ?= "3_21_00_04"
PREFERRED_VERSION_ti-firmware-dm814x-ezsdk  ?= "5_04_00_11"
PREFERRED_VERSION_ti-media-controller-utils ?= "2_03_01_14"

PREFERRED_VERSION_ti-c674x-aaclcdec         ?= "01_41_00_00"
PREFERRED_VERSION_ti-codec-engine           ?= "3_21_02_25"
PREFERRED_VERSION_ti-edma3lld               ?= "02_11_02_04"
PREFERRED_VERSION_ti-framework-components   ?= "3_21_03_34"
PREFERRED_VERSION_ti-osal                   ?= "1_21_01_08"
PREFERRED_VERSION_ti-xdais                  ?= "7_21_01_07"
PREFERRED_VERSION_ti-uia                    ?= "1_00_03_25"
PREFERRED_VERSION_ti-slog                   ?= "04_00_00_02"
PREFERRED_VERSION_ti-omx                    ?= "05_02_00_30"


