####
# EZSDK Version 5.3.0.9
####
PREFERRED_VERSION_linux-omap3               ?= "2.6.37-ti814xpsp-${PSPRELEASE}"
PREFERRED_VERSION_u-boot                    ?= "2010.06-psp${PSPRELEASE}"
PREFERRED_VERSION_u-boot-min-sd             ?= "2010.06-psp${PSPRELEASE}"
PREFERRED_VERSION_ti-ipc                    ?= "1_23_05_40"
PREFERRED_VERSION_ti-cgt6x                  ?= "7_3_1"
PREFERRED_VERSION_xdctools                  ?= "3_22_04_46"
PREFERRED_VERSION_sysbios                   ?= "6_32_05_54"
PREFERRED_VERSION_syslink                   ?= "2_00_04_83"
PREFERRED_VERSION_ti-linuxutils             ?= "3_21_00_04"
PREFERRED_VERSION_ti-firmware-dm814x-ezsdk  ?= "5_03_00_09"
PREFERRED_VERSION_ti-media-controller-utils ?= "2_03_00_12"

PREFERRED_VERSION_ti-c674x-aaclcdec         ?= "01_41_00_00"
PREFERRED_VERSION_ti-codec-engine           ?= "3_21_01_23"
PREFERRED_VERSION_ti-edma3lld               ?= "2_11_02_04"
PREFERRED_VERSION_ti-framework-components   ?= "3_21_02_32"
PREFERRED_VERSION_ti-osal                   ?= "1_21_01_08"
PREFERRED_VERSION_ti-xdais                  ?= "7_21_01_07"
PREFERRED_VERSION_ti-uia                    ?= "1_00_03_25"
PREFERRED_VERSION_ti-slog                   ?= "04_00_00_02"
PREFERRED_VERSION_ti-omx                    ?= "05_02_00_26"

