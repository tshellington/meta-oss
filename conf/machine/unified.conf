#@TYPE: Machine
#@NAME: unified
#@DESCRIPTION: Unified machine configuration for a Devon IT thinclient

MODELS ?= "TC2,TC3,TC5,TC5c,TC5v,TC6,OptiPlex FX170,OptiPlex FX130,V800"

IMAGE_DEVICE_TABLES = " \
  files/device_table-minimal.txt \
  files/device_table_add-loop.txt \
  files/device_table_add-scsi.txt \
"

MACHINE_EXTRA_RDEPENDS += " kernel-modules linux-firmware-radeon kernel-module-chrome9"
MACHINE_EXTRA_DEPENDS += "grub grub-native v86d"
MACHINE_DRI_MODULES = "i915,i965,radeon,r200"

PREFERRED_PROVIDER_virtual/bootloader = "grub"
PREFERRED_PROVIDER_virtual/kernel = "linux-official"
PREFERRED_VERSION_linux-official = "3.2"
PREFERRED_PROVIDER_aufs-module = "aufs3-module"
PREFERRED_PROVIDER_aufs-util = "aufs3-util"
PREFERRED_VERSION_aufs3-util = "3.0"
PREFERRED_VERSION_aufs3-module = "3.2"
 
AUFS_VERSION = "38"

require conf/machine/include/tune-i586.inc

XSERVER += "\
            xf86-input-vmmouse \
            xf86-video-vmware \
	    xf86-video-intel \
            xf86-video-ati \
            xf86-video-chrome9 \
            xf86-video-fbdev \
            libva \
            libdrm-kms \
            libgl \
            mesa-dri-driver-swrast \
            mesa-dri-driver-i915 \
            mesa-dri-driver-i965 \
            mesa-dri-driver-radeon \
	    mesa-dri-driver-r300 \
	    mesa-dri-driver-r600 \
           "

PCMCIA_MANAGER = "pcmciautils"
GUI_MACHINE_CLASS = "bigscreen"
MACHINE_FEATURES = "kernel26 screen keyboard pci usbhost ext2 ext3 x86 \
                    acpi serial usbgadget alsa vfat bluetooth"

GLIBC_ADDONS ?= "nptl"
GLIBC_EXTRA_OECONF += "--with-tls"

IMAGE_FSTYPES ?= "tar.bz2 ext2"

INITRAMFS_FMT = "cpio.lzma"
ROOTFS_FMT = "squashfs"

ROOT_FLASH_SIZE = "230"
IMAGE_ROOTFS_SIZE_ext2 ?= "230000"

# Don't include kernels in standard images
RDEPENDS_kernel-base = ""

CONSOLE_TTY = "${@base_conditional('DISTRO_TYPE', 'release', 'tty2', 'tty1',d)}"
SERIAL_PORT = "ttyS0"
#BOOTARGS = "console=ttyS0 live-media=/dev/sda1 "
BOOTARGS = " live-media=/dev/sda1 quickusbmodules skipconfig console=${CONSOLE_TTY}"

KERNEL_IMAGETYPE = "bzImage"
