DESCRIPTION = "An initramfs module for booting into a live system"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-3.0;md5=c79ff39f19dfec6d293b95dea7b07891"

PACKAGES += "initramfs-module-${PN}"
PACKAGES_DYNAMIC = "live-boot-premount-module-* live-boot-bottom-module-*"

SRC_URI = " \
             http://live.debian.net/archive/packages/live-boot/${PV}-1/live-boot_${PV}.orig.tar.gz \
             file://functions.sh  \
             file://live-init.sh  \
             file://live-boot.init \
             file://live-snapshot.exclude_list \
          "

PR = "r3"
S = "${WORKDIR}/live-boot-${PV}"

RDEPENDS_initramfs-module-${PN} = " \
                  cpio eject busybox \
                  aufs-util udev \
                  aufs-module \
                  util-linux-blkid udev procps \
                  cifs nfs-utils-client \
                  module-init-tools \
                 "

# These need to stay soft
RRECOMMENDS_initramfs-module-${PN} = " \
                     kernel-module-fat \
                     kernel-module-msdos \
                     kernel-module-vfat \
                     kernel-module-nls-utf8 \
                     kernel-module-nls-cp437 \
                     kernel-module-nls-iso8859-1 \
                     kernel-module-ext4 \
                     kernel-module-nfs \
                     kernel-module-cifs \
                     kernel-module-loop \
                     kernel-module-squashfs \
                    "

RDEPENDS_${PN} = "eject file ldd rsync"

do_install() {
  install -d ${D}/scripts/live-bottom
  install -d ${D}/scripts/live-premount
  install -d ${D}/init.d

  install -m 0755 scripts/live ${D}/scripts/live
  install -m 0755 scripts/live-functions ${D}/scripts/live-functions
  install -m 0755 ${WORKDIR}/functions.sh ${D}/scripts/functions
  install -m 0755 scripts/live-helpers ${D}/scripts/live-helpers
  install -m 0755 scripts/live-bottom/* ${D}/scripts/live-bottom
  install -m 0755 scripts/live-premount/* ${D}/scripts/live-premount
  install -m 0755 ${WORKDIR}/live-init.sh ${D}/init.d/03-live

  install -d ${D}/dev
  install -d ${D}/proc 
  install -d ${D}/sys 
  install -d ${D}/mnt 
  install -d ${D}/tmp 
  install -d ${D}/root 
  install -d ${D}/var/lock  

  install -d ${D}${sbindir}
  install -d ${D}/usr/share/initramfs-tools/scripts
  install -d ${D}${sysconfdir}/init.d
  install -d ${D}/etc/live/boot.d

  install -m 0755 bin/* ${D}${sbindir}
  install -m 0755 scripts/live-helpers ${D}/usr/share/initramfs-tools/scripts/live-helpers
  install -m 0755 scripts/live-functions ${D}/usr/share/initramfs-tools/scripts/live-functions

  install -m 0755 ${WORKDIR}/live-snapshot.exclude_list ${D}${sysconfdir}

}

python populate_packages_prepend () {
        live_boot_bottom_dir = bb.data.expand('/scripts/live-bottom', d)
        do_split_packages(d, live_boot_bottom_dir, '^[0-9]+(.*)$', 'live-boot-bottom-module-%s', 'Live boot bottom module for %s', extra_depends='initramfs-module-live-boot')

        live_boot_premount_dir = bb.data.expand('/scripts/live-premount', d)
        do_split_packages(d, live_boot_premount_dir, '^(.*)$', 'live-boot-premount-module-%s', 'Live boot premount module for %s', extra_depends='initramfs-module-live-boot')
}
  
FILES_initramfs-module-${PN} =  " \
                /init.d/* \
                /scripts/live-helpers \
                /scripts/live-functions \
                /scripts/live \
                /scripts/functions \
                /dev \
                /proc \
                /sys \
                /mnt \
                /tmp \
                /root \
                /var/lock \
               "

FILES_${PN} = " \
                /etc/live-snapshot.exclude_list \
                /etc/init.d/live-boot  \
                /usr/sbin/* \
                /usr/share/initramfs-tools/scripts/* \
              "

inherit update-rc.d

INITSCRIPT_NAME = "live-boot"
INITSCRIPT_PARAMS = "start 25 0 6 ."

