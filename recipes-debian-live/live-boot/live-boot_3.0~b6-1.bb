DESCRIPTION = "An initramfs module for booting into a live system"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-3.0;md5=c79ff39f19dfec6d293b95dea7b07891"

PACKAGES += "initramfs-module-${PN}"

SRC_URI = " \
             http://live.debian.net/archive/packages/live-boot/3.0~b6-1/live-boot_3.0~b6.orig.tar.xz \
             file://functions.sh \
             file://live-init.sh  \
             file://0001-Use-dynamic-root-mount.patch \
             file://0002-Live-media-persistence.patch \    
             file://0003-method-for-scanning-directories-of-configuration-fil.patch \
             file://0004-search-LIVE_MEDIA_PATH-for-configs.patch \
             file://0005-do_union-varargs.patch \
             file://0006-Layer-multiple-rootfs-for-union-overlays.patch \
          "

SRC_URI[md5sum] = "c2bff56fd0345f051d554dcfc1c17f0e"
SRC_URI[sha256sum] = "e5a680022d5891032ba99aaa3ffc7a0bca7f41fb9258d79e8e546281313782f4"

PR = "r26"
S = "${WORKDIR}/live-boot-3.0~b6"

RDEPENDS_initramfs-module-${PN} = " \
                  cpio eject busybox \
                  aufs-util udev \
                  aufs-module \
                  util-linux-blkid udev procps \
                  cifs nfs-utils-client \
                  module-init-tools \
                 "

# These need to stay soft
RRECOMMENDS_initramfs-module-${PN} = " \
                     kernel-module-fat \
                     kernel-module-msdos \
                     kernel-module-vfat \
                     kernel-module-nls-utf8 \
                     kernel-module-nls-cp437 \
                     kernel-module-nls-iso8859-1 \
                     kernel-module-ext4 \
                     kernel-module-nfs \
                     kernel-module-cifs \
                     kernel-module-loop \
                     kernel-module-squashfs \
                    "

RDEPENDS_${PN} = "eject file ldd rsync"

do_install() {
  make DESTDIR=${D} install

  install -d ${D}/init.d
  install -m 0755 ${WORKDIR}/live-init.sh ${D}/init.d/03-live

  install -d ${D}/scripts
  install -m 0755 ${WORKDIR}/functions.sh ${D}/scripts/functions

  install -d ${D}/dev
  install -d ${D}/proc 
  install -d ${D}/sys 
  install -d ${D}/mnt 
  install -d ${D}/tmp 
  install -d ${D}/root 
  install -d ${D}/var/lock  

}

FILES_initramfs-module-${PN} =  " \
                /init.d \
                /scripts \
                /lib \
                /dev \
                /proc \
                /sys \
                /mnt \
                /tmp \
                /root \
                /var/lock \
               "

FILES_${PN} = " \
                /usr/share \
              "

