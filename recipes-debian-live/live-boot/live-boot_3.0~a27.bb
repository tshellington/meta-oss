require live-boot.inc

SRC_URI[md5sum] = "2339a74ee916a19b6e42be2f751ce1ff"
SRC_URI[sha256sum] = "e39568aceb81dc76e9fe7abd61f4ed7c26379daf6655cb9bd8409ac5cda10d23"

SRC_URI += " \
             file://0001-Parse-live-configurations-from-live-media.patch \
             file://0002-Fix-to-allow-for-multiple-rof-when-exposed-roots-is-.patch \
             file://0003-Fix-regexp-to-use-word-boundries.patch \
             file://0004-Return-backing-if-its-already-mounted-so-that-the-ca.patch \
             file://0001-Clean-rofslist-paths.patch \
             file://test-live-image.patch \
           "




