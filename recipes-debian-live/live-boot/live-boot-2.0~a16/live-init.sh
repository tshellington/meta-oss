#!/bin/sh

export rootmnt=/root

mkdir /dev/pts
mount -t devpts -o noexec,nosuid,gid=5,mode=0620 none /dev/pts || true
 
. /scripts/live

modprobe -q squashfs 
modprobe -q loop 

mountroot

BOOT_ROOT="${rootmnt}"

if [ -d $BOOT_ROOT/live/image ]; then
   umount /dev/pts 
else
   exit 1
fi

for MNT in $(mount | grep "\-backing" | cut -d ' ' -f 3)
do
    mkdir -p  $BOOT_ROOT/$MNT
    mount -o move $MNT $BOOT_ROOT/$MNT
done

for MNT in $(mount |grep -v overlay | grep "/dev/loop" | cut -d ' ' -f 3)
do
    mkdir -p  $BOOT_ROOT/$MNT
    mount -o move $MNT $BOOT_ROOT/$MNT
done
