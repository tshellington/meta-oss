#!/bin/sh

live_enabled() {
    return 0
}


live_run() {

    mkdir /dev/pts
    mount -t devpts -o noexec,nosuid,gid=5,mode=0620 none /dev/pts || true
 
    source /lib/live/boot.sh

    load_kernel_module squashfs
    load_kernel_module loop

    export rootmnt=${ROOTFS_DIR}
    mountroot

    if [ -d ${rootmnt}/lib/live/image ]; then
       umount /dev/pts 
    else
       fatal "ERROR: liveboot failed"
    fi
}
