#!/bin/sh

live_enabled() {
    return 0
}


live_run() {

    mkdir /dev/pts
    mount -t devpts -o noexec,nosuid,gid=5,mode=0620 none /dev/pts || true
 
    source /scripts/live

    load_kernel_module squashfs
    load_kernel_module loop

    export rootmnt=${ROOTFS_DIR}
    mountroot

    if [ -d $BOOT_ROOT/live/image ]; then
       umount /dev/pts 
    else
       fatal "ERROR: liveboot failed"
    fi
}
