#!/bin/sh

display_status() {
        logger "${1}"
}

_log_msg()
{
        logger "$@"
}

log_success_msg()
{
        _log_msg "Success: $@"
}

log_failure_msg()
{
        _log_msg "Failure: $@"
}

log_warning_msg()
{
        _log_msg "Warning: $@"
}

log_begin_msg()
{
        _log_msg "Begin: $@ ..."
}

log_end_msg()
{
        _log_msg "Done."
}

maybe_break()
{
        if echo "${break:-}" | egrep -q "(,|^)$1(,|$)"; then
                panic "Spawning shell within the initramfs"
        fi
}

panic()
{
        chvt 1

        # Disallow console access
        if [ -n "${panic}" ]; then
                sleep ${panic}
                reboot
        fi

        echo $@
        PS1='(initramfs) ' /bin/sh -i </dev/console >/dev/console 2>&1
}

run_scripts()
{
        initdir=${1}
        [ ! -d ${initdir} ] && return

        for cs_x in ${initdir}/*; do
            ${cs_x}
        done
}

