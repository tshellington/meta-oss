#!/bin/sh

### BEGIN INIT INFO
# Provides:            live-boot
# Required-Start:      $syslog bootmisc
# Required-Stop:
# Should-Start:                $local_fs
# Should-Stop:         halt reboot
# X-Stop-After:                umountroot
# Default-Start:       S
# Default-Stop:                0 6
# Short-Description:   live-boot init script
# Description:         Resyncs snapshots, evantually caches files in order to
#                      let remove the media.
### END INIT INFO

# Authors: Tollef Fog Heen <tfheen@canonical.com>
#         Marco Amadori <marco.amadori@gmail.com>

PATH=/usr/sbin:/usr/bin:/sbin:/bin
NAME=live-boot
SCRIPTNAME=/etc/init.d/${NAME}
DO_SNAPSHOT=/usr/sbin/live-snapshot
SNAPSHOT_CONF="/etc/live/boot.d/snapshot.conf"

# Exit if system was not booted by live-boot
grep -qs boot=live /proc/cmdline || exit 0

# Read snapshot configuration variables
[ -r ${SNAPSHOT_CONF} ] && . ${SNAPSHOT_CONF}

# Load the VERBOSE setting and other rcS variables
[ -f /etc/default/rcS ] && . /etc/default/rcS

[ -f /etc/default/splashfuncs ] && . /etc/default/splashfuncs


get_boot_device()
{
       # search in /proc/mounts for the device that is mounted at /live/image
       while read DEVICE MOUNT REST
       do
               if [ "${MOUNT}" = "/live/image" ]
               then
                       echo "${DEVICE}"
                       exit 0
               fi
       done < /proc/mounts
}

device_is_USB_flash_drive()
{
       # remove leading "/dev/" and all trailing numbers from input
       DEVICE=$(expr substr ${1} 6 3)

       # check that device starts with "sd"
       [ "$(expr substr ${DEVICE} 1 2)" != "sd" ] && return 1

       # check that the device is an USB device
       if readlink /sys/block/${DEVICE} | grep -q usb ||
          readlink /sys/block/${DEVICE}/device | grep -q usb # linux < 2.6.29
       then
               return 0
       fi

       return 1
}

do_stop ()
{
       if ! grep -qs nopersistent /proc/cmdline && grep -qs persistent /proc/cmdline
       then
               # ROOTSNAP and HOMESNAP are defined in ${SNAPSHOT_CONF} file
               if [ ! -z "${ROOTSNAP}" ]
               then
                       ${DO_SNAPSHOT} --resync-string="${ROOTSNAP}"
               fi

               if [ ! -z "${HOMESNAP}" ]
               then
                       ${DO_SNAPSHOT} --resync-string="${HOMESNAP}"
               fi
       fi

       sync
       mount -no remount,ro /
       mount -no remount,ro /live/overlay
       # check for netboot
       if [ ! -z "${NETBOOT}" ] || grep -qs netboot /proc/cmdline || grep -qsi root=/dev/nfs /proc/cmdline  || grep -qsi root=/dev/cifs /proc/cmdline
       then
               return 0
       fi

       # Don't prompt to eject the SD card on Babbage board, where we reuse it
       # as a quasi-boot-floppy. Technically this uses a bit of ubiquity
       # (archdetect), but since this is mostly only relevant for
       # installations, who cares ...
       if type archdetect >/dev/null 2>&1
       then
               subarch="$(archdetect)"

               case $subarch in
                       arm*/imx51)
                               return 0
                               ;;
               esac
       fi

       prompt=1
       if grep -qs noprompt /proc/cmdline
       then
               prompt=
       fi

       for x in $(cat /proc/cmdline)
       do
               case ${x} in
                       quickreboot)
                               QUICKREBOOT="Yes"
                               ;;
               esac
       done

       if [ -z ${QUICKREBOOT} ]
       then

               # Exit if the system was booted from an ISO image rather than a physical CD
               grep -qs find_iso= /proc/cmdline && return 0
               # TODO: i18n
               BOOT_DEVICE="$(get_boot_device)"

               if device_is_USB_flash_drive ${BOOT_DEVICE}
               then
                       # do NOT eject USB flash drives!
                       # otherwise rebooting with most USB flash drives
                       # failes because they actually remember the
                       # "ejected" state even after reboot
                       MESSAGE="Please remove the USB flash drive"
               else
                       # ejecting is a very good idea here
                       MESSAGE="Please remove the disc, close the the tray (if any)"

                       if [ -x /usr/bin/eject ]
                       then
                               eject -p -m /live/image >/dev/null 2>&1
                       fi

               fi

               [ "$prompt" ] || return 0

		# balls
               if [ -x /usr/bin/psplash ] 
               then
                      # plymouth message --text="${MESSAGE} and press ENTER to continue:"
                      # plymouth watch-keystroke > /dev/null
                       stty sane < /dev/console
                       status_text "${MESSAGE} and press ENTER to continue:"
                       read x < /dev/console
               else
                       stty sane < /dev/console

                       printf "\n\n${MESSAGE} and press ENTER to continue:" > /dev/console

                       read x < /dev/console
               fi
       fi
}

case "${1}" in
       restart|reload|force-reload|status)
               exit 0
               ;;
       start)
               if [ -f /live/root.pid ] ; then
                       cat /live/root.pid >> /var/run/sendsigs.omit
               fi
               exit 0
               ;;

       stop)
               do_stop

               case "${?}" in
                       0|1)
                               exit 0
                               ;;

                       2)
                               exit 1
                               ;;
               esac

               ;;

       *)
               echo "Usage: ${SCRIPTNAME} {start|stop|restart|force-reload}" >&2
               exit 3
               ;;
esac

