DESCRIPTION = "Snapshot persistence for DeTOS live-boot"
SECTION = "base"
PRIORITY = "required"

LICENSE = "Proprietary"

PACKAGES += "initramfs-module-${PN}"

LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

PR = "r4"

SRC_URI = " \
           file://live-snapshot.sh \
           file://live-snapshot-restore.sh \
          "

FILES_${PN} = "/usr/bin"
FILES_initramfs-module-${PN} = "/init.d"
RDEPENDS_initramfs-module-${PN} += "initramfs-module-live-boot"

do_install () {
  install -d ${D}/usr/bin
  install -d ${D}/init.d
  install -m 0755 ${WORKDIR}/live-snapshot.sh ${D}/usr/bin/live-snapshot
  install -m 0755 ${WORKDIR}/live-snapshot-restore.sh ${D}/init.d/04-live_snapshot_restore
}
