#!/bin/sh 
EXCLUDES=${1}
INCLUDES=${2}
OUTPUT=${3}

if [ -e ${EXCLUDES} -a -e ${INCLUDES} ]
then
    tar -S -cv -z -X ${EXCLUDES} --one-file-system --ignore-failed-read -C /lib/live/overlay/ -f ${OUTPUT} $(cat ${INCLUDES})
    exit $?
else
    exit 1
fi
