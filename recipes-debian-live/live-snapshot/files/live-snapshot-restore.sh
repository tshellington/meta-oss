#!/bin/sh

live_snapshot_restore_enabled() {
    return 0
}

live_snapshot_restore_run() {
    SNAPSHOT=${rootmnt}/lib/live/image/live-sn.tar.gz
    if [ ${PERSISTENCE} == "Yes" -a ${PERSISTENCE_METHOD} == "snapshot" -a -e ${SNAPSHOT} ]; then
        tar -zxf ${SNAPSHOT} -C  ${rootmnt}
    fi
}
