DESCRIPTION = "Liveconfig modules for DeTOS"
SECTION = "base"
PRIORITY = "required"
RDEPENDS_${PN} = "live-config"

LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

PR = "r4.5"

SRC_URI = " \
           file://pango-setup.sh \
           file://gdkpixbuf-setup.sh \
           file://ramz-setup.sh \
           file://intel-setup.sh \
           file://update-partition-setup.sh \
           file://usb-fstab-setup.sh \
           file://hostname-setup.sh \
           file://agent-setup.sh \
           file://gtk-uim-setup.sh \
           file://check-blaster.sh \
           file://dm814x-loadFilesFromSpi.sh \
          "

FILES_${PN} = "/lib/live/config"

do_install () {
  install -d ${D}/lib/live/config
  install -m 0755 ${WORKDIR}/hostname-setup.sh ${D}/lib/live/config/00-hostname-setup
  install -m 0755 ${WORKDIR}/ramz-setup.sh ${D}/lib/live/config/09-ramz-setup
  install -m 0755 ${WORKDIR}/pango-setup.sh ${D}/lib/live/config/12-pango-setup
  install -m 0755 ${WORKDIR}/gdkpixbuf-setup.sh ${D}/lib/live/config/13-gdkpixbuf-setup
  install -m 0755 ${WORKDIR}/intel-setup.sh ${D}/lib/live/config/16-intel-setup
  install -m 0755 ${WORKDIR}/update-partition-setup.sh ${D}/lib/live/config/17-update-partition-setup
  install -m 0755 ${WORKDIR}/usb-fstab-setup.sh ${D}/lib/live/config/18-usb-fstab-setup
  install -m 0755 ${WORKDIR}/agent-setup.sh ${D}/lib/live/config/19-agent-setup
  install -m 0755 ${WORKDIR}/gtk-uim-setup.sh ${D}/lib/live/config/20-gtk-uim-setup
  install -m 0755 ${WORKDIR}/check-blaster.sh ${D}/lib/live/config/21-check-blaster
}

do_install_append_dm814x-fx1 () {
  install -m 0755 ${WORKDIR}/dm814x-loadFilesFromSpi.sh ${D}/lib/live/config/22-dm814x-loadFilesFromSpi
}
