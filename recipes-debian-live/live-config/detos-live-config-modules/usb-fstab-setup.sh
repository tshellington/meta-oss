#!/bin/sh

UsbFstab_setup ()
{
        if [ -e /var/lib/live/config/usb-fstab-setup ]
        then
                return
        fi

        echo -n "usb-fstab"

        Configure_UsbFstab_setup

        touch /var/lib/live/config/usb-fstab-setup
}

Configure_UsbFstab_setup ()
{
        if ! grep -q usbfs /etc/fstab
        then
                GID=$(grep "usb:" /etc/group | cut -d ":" -f 3)
                echo "none /proc/bus/usb usbfs auto,listuid=0,listgid=${GID},listmode=0664,busuid=0,busgid=${GID},busmode=0775,devuid=0,devgid=${GID},devmode=0664 0 0" >> /etc/fstab 
        fi
}

UsbFstab_setup
