#!/bin/sh


QueryImmodules_setup ()
{
        # Checking if package is installed or already configured
        if [ -e /var/lib/live/config/query-immodules-setup ]
        then
                return
        fi

        echo -n " query-immodules-setup"

        Configure_QueryImmodules_setup

        touch /var/lib/live/config/query-immodules-setup
}

Configure_QueryImmodules_setup ()
{
        /usr/bin/gtk-query-immodules-2.0 > /etc/gtk-2.0/gtk.immodules
}

QueryImmodules_setup

