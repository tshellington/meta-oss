#!/bin/sh
UpdatePart_setup ()
{
        # Checking if package is installed or already configured
        if [ -e /var/lib/live/config/updatepart-setup ]
        then
                return
        fi

        echo -n " updatepart-setup"

        Configure_UpdatePart_setup

}

Configure_UpdatePart_setup ()
{
  grep -qs zeroconfig /proc/cmdline && return 0

  [ ! -d /update ] && mkdir /update
  mount /dev/disk/by-label/boot /update
  #copy initrd and kernel from the agent directory if they are newer
  rsync -au /opt/devonit/agent/initrd/* /update/boot
}

UpdatePart_setup
