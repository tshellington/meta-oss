#!/bin/sh

if [ -b /dev/mtdblock0 ]; then
    mkdir /mnt/mtdblock0
    mount -t jffs2 /dev/mtdblock0 /mnt/mtdblock0
    if [ $? != 0 ]; then
        echo "Failed to mount /dev/mtdblock0 at /mnt/mtdblock0."
    fi
    cp /mnt/mtdblock0/serialnumber /etc/devonit/serialnumber
    cp /mnt/mtdblock0/nvs-file /lib/firmware/ti-connectivity/wl1271-nvs.bin
    if [ $? != 0 ]; then
        echo "Failed to copy /mnt/mtdblock0/mtdblock0/nvs-file to /lib/firmware/ti-connectivity/wl1271-nvs.bin."
    fi
    sync
    if [ $? != 0 ]; then
        echo "Failed to execute sync command."
    fi
    umount /mnt/mtdblock0
    if [ $? != 0 ]; then
        echo "Failed to unmount /mnt/mtdblock0."
    fi
else
    echo "/dev/mtdblock0 is not a block device."
fi
