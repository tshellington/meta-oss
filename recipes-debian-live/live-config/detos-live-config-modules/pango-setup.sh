#!/bin/sh
Pango_setup ()
{
        if [ -e /var/lib/live/config/pango-setup ]
        then
                return
        fi

        echo -n " pango-setup"

        Configure_pango_setup

        touch /var/lib/live/config/pango-setup
}

Configure_pango_setup ()
{
        /usr/bin/pango-querymodules > /etc/pango/pango.modules
}

Pango_setup