#!/bin/sh

IntelVideo_setup ()
{
        if [ -e /var/lib/live/config/intelvideo-setup ]
        then
                return
        fi

        echo -n "Configure Intel Video"

        Configure_IntelVideo_setup

        touch /var/lib/live/config/intelvideo-setup
}

Configure_Xorg ()
{
    cat > /etc/X11/xorg.conf.d/intel.conf << EOF
Section "Device"
        Identifier      "Configured Video Device"
        Option           "Monitor-TV1" "TV1"
EndSection

Section "Monitor"
        Identifier      "TV1"
        Option          "Ignore" "True"
EndSection

EOF

}

Configure_IntelVideo_setup ()
{
       CHIPID=$(lspci -nn | grep "\[0300\]\:" | sed -n 's/.*[0-9a-z]\{4\}:\([0-9a-z]\{4\}\)].*/\1/gp')
       case "$CHIPID" in
            27ae)
                  Configure_Xorg $CHIPID
            ;;
            *) ;;
       esac
}

IntelVideo_setup
