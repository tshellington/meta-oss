#!/bin/sh


GdkPixbuf_setup ()
{
        # Checking if package is installed or already configured
        if [ -e /var/lib/live/config/gdkpixbuf-setup ]
        then
                return
        fi

        echo -n " gdkpixbuf-setup"

        Configure_gdkpixbuf_setup

}

Configure_gdkpixbuf_setup ()
{
        /usr/bin/gdk-pixbuf-query-loaders > /usr/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache
}

GdkPixbuf_setup

