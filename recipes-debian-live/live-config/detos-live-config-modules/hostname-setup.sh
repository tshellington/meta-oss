#!/bin/sh

Hostname ()
{
        # Checking if package is installed or already configured
        if [ -e /var/lib/live/config/hostname ]
        then
                return
        fi

        echo -n " hostname"

        Configure_hostname

        touch /var/lib/live/config/hostname
}

CreateHostfile ()
{

cat > /etc/hosts << EOF
127.0.0.1       localhost ${LIVE_HOSTNAME}
::1             localhost ip6-localhost ip6-loopback
fe00::0         ip6-localnet
ff00::0         ip6-mcastprefix
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
EOF
  
}

Configure_hostname ()
{
        if [ -z "${LIVE_HOSTNAME}" ]; then
           LIVE_HOSTNAME="$(/sbin/ifconfig eth0 |  grep HWaddr | awk '{print $5}' | tr ":" "-")"
        fi

        echo "${LIVE_HOSTNAME}" > /etc/hostname

        hostname "${LIVE_HOSTNAME}"        
}

Hostname
