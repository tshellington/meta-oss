#!/bin/sh

Agent_setup ()
{
        echo -n " agent-setup"

        Configure_agent_setup
}

Configure_agent_setup ()
{
     AGENT_SQUASHFS=/lib/live/image/live/agent.pkg
     AGENT_MOUNTPOINT="/opt/devonit/agent"

     if [ -e "${AGENT_SQUASHFS}" ]; then
        mount -o loop "${AGENT_SQUASHFS}" "${AGENT_MOUNTPOINT}"

        # Execute update script if present
        if [ -x "${AGENT_MOUNTPOINT}/mount.sh" ]; then
           "${AGENT_MOUNTPOINT}/mount.sh"
        fi
     fi
}

Agent_setup
