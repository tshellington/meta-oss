#!/bin/sh
RamZ_setup ()
{
        # Checking if package is installed or already configured
        if [ -e /var/lib/live/config/ramz-setup ]
        then
                return
        fi

        echo -n " ramz-setup"

        Configure_RamZ_setup

}

Configure_RamZ_setup ()
{
        mem_total_kb=$(grep MemTotal /proc/meminfo | grep -E --only-matching '[[:digit:]]+')
        mem_total=$((mem_total_kb * 1024))
        modprobe zram
        echo $((mem_total / 2)) > /sys/block/zram0/disksize
        mkswap /dev/zram0
        swapon -p 100 /dev/zram0
}

RamZ_setup
