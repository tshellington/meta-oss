#!/bin/sh
CheckBlaster_setup ()
{
        # Checking if package is installed or already configured
        if [ -e /var/lib/live/config/checkblaster-setup ]
        then
                return
        fi

        echo -n " checkblaster-setup"

        Configure_CheckBlaster_setup

        touch /var/lib/live/config/checkblaster-setup
}

Configure_CheckBlaster_setup ()
{
  if [ -e "/live/image/boot.ini" ]; then
    touch /tmp/blaster-coinstall
  fi
}

CheckBlaster_setup
