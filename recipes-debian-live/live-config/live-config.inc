SRC_URI = " \
            http://live.debian.net/archive/packages/live-config/${PV}-1/live-config_${PV}.orig.tar.gz \
            file://live-config.service \
            file://fix-hostname.patch \
          "
PR = "r8"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

S = "${WORKDIR}/live-config-${PV}"

DESCRIPTION = "Debian live boot system"

inherit systemd

PACKAGES_DYNAMIC = "live-config-module-*"
SYSTEMD_PACKAGES = "${PN}-systemd"
SYSTEMD_SERVICE_${PN}-systemd = "${PN}.service"

do_install() {
        # Installing scripts
        install -d ${D}/lib/live
        install -m 0755 scripts/config.sh ${D}/lib/live
        install -d ${D}/lib/live/config
        install -m 0755 scripts/config/* ${D}/lib/live/config
        install -d ${D}/var/lib/live/config
        touch  ${D}/var/lib/live/config/.live-config
}

FILES_${PN} += " /lib/live/config.sh  /var/lib/live/config/.live-config /etc/init.d/live-config "

python populate_packages_prepend () {
        live_config_dir = bb.data.expand('/lib/live/config', d)
        do_split_packages(d, live_config_dir, '^[0-9]+-(.*)$', 'live-config-module-%s', 'Live config module module for %s', extra_depends='')
}

