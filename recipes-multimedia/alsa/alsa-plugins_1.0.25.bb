DESCRIPTION = "ALSA Plugins"
HOMEPAGE = "http://www.alsa-project.org"
SECTION = "multimedia/alsa/plugins"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://COPYING;md5=7fbc338309ac38fefcd64b04bb903e34"
DEPENDS = "alsa-lib pulseaudio ffmpeg"
PR = "r2"

SRC_URI = "http://mirrors.zerg.biz/alsa/plugins/alsa-plugins-${PV}.tar.bz2"

inherit autotools

CFLAGS += "-lavutil"

PACKAGES_DYNAMIC = "libasound-module*"

python populate_packages_prepend() {
        plugindir = bb.data.expand('${libdir}/alsa-lib/', d)
        do_split_packages(d, plugindir, '^libasound_module_(.*)\.so$', 'libasound-module-%s', 'Alsa plugin for %s', extra_depends='' )
}

FILES_${PN}-dev += "${libdir}/alsa-lib/libasound*.a ${libdir}/alsa-lib/libasound*.la"
FILES_${PN}-dbg += "${libdir}/alsa-lib/.debug"

SRC_URI[md5sum] = "038c023eaa51171f018fbf7141255185"
SRC_URI[sha256sum] = "a0e374fd6d5ee9683473a5b6e73dadde61d54851065ed670d6627d344b565aab"
