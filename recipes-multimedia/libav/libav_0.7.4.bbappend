FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
PRINC = "2"

EXTRA_OECONF_append_88AP510 = "\
        --extra-cflags=-mfpu=vfpv3-d16\
        --disable-neon \
"
