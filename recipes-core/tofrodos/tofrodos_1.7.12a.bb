DESCRIPTION = "Tofrodos is a text file conversion utility that converts ASCII files between the MSDOS and unix format"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://../COPYING;md5=8ca43cbc842c2336e835926c2166c28b"
PR = "r2"

SRC_URI = "http://tofrodos.sourceforge.net/download/tofrodos-${PV}.tar.gz \
           file://cross.patch;striplevel=2"

S = "${WORKDIR}/${PN}/src"

do_install() {
	install -d ${D}/usr/man/man1
	install -d ${D}/${bindir}
	oe_runmake DESTDIR=${D} install
	cd ${D}/${bindir}
}

FILES_${PN} = "${bindir}/fromdos ${bindir}/todos"

pkg_postinst_${PN} () {
#!/bin/sh
    update-alternatives --install ${bindir}/dos2unix dos2unix fromdos 100
    update-alternatives --install ${bindir}/unix2dos unix2dos todos 100
}

pkg_prerm_${PN} () {
 #!/bin/sh
   update-alternatives --remove dos2unix fromdos
   update-alternatives --remove unix2dos todos
}

SRC_URI[md5sum] = "219c03d7c58975b335cdb5201338125a"
SRC_URI[sha256sum] = "3098af78325486b99116c65c9f9bbbbfb3dfbeab1ab1e63a8da79550a5af6a08"

