DEPENDS_append_unified = " cups libxinerama "
DEPENDS_append_unifiedx64 = " cups libxinerama "
DEPENDS_append_vt6067 = " cups libxinerama "
DEPENDS_append_88AP510 = " cups libxinerama "
DEPENDS_append_dm814x-fx1 = " cups libxinerama "
DEPENDS_append_dm814x-evm = " cups libxinerama "

EXTRA_OECONF = "--without-libtiff --without-libjasper --enable-xkb --disable-glibtest --enable-cups"
PR =. "devon4-"
