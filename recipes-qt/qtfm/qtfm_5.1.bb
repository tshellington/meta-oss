DESCRIPTION = "Qt File Manager"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

inherit qmake2 qt4x11

# QMAKE_PROFILES = "${PN}.pro"

PR = "1"

SRC_URI = " \
	${DEVON_MIRRORS}/vendor/qtfm/qtfm-${PV}.tar.gz;name=archive \
	file://qtfm-restricted-path.patch \
	"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}

SRC_URI[archive.md5sum] = "aecdf1fd783371248215ad2bf853dd37"
SRC_URI[archive.sha256sum] = "41a6e0fd409e0d09f6b7d363e860294f2a568c32e211d91d8f61270604add2a5"

