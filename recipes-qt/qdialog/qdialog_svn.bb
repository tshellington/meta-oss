DESCRIPTION = "Qt version of dialog"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

RDEPENDS = " \
             qt4-plugin-imageformat-svg \
             qt4-plugin-imageformat-jpeg  \
             qt4-plugin-iconengine-svgicon \
           "

inherit qmake2 qt4x11

QMAKE_PROFILES = "${PN}.pro"

PR = "1"
PV = "${SRCPV}"

SRCREV = "1101"
SRC_URI = "svn://svn.geiseri.com/svn/projects;module=${PN};proto=http"

S = "${WORKDIR}/${PN}"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}

