DESCRIPTION = "Desktop widgets for QtQuick"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://header.BSD;md5sum=6127ef7232170f61b7c5f4da50768c27"

PR = "r1"
PR_append = "+gitr${SRCREV}"

inherit qmake2 qt4x11 pkgconfig

SRCREV = "ed5131c6ad53cbf16532cb37cf48b423d860a13c"
SRC_URI = "git://git.gitorious.org/qt-components/desktop.git;protocol=http"

S = "${WORKDIR}/git"

QMAKE_PROFILES = "desktop.pro"

do_configure_prepend() {

   for PRO in $(find ${S} -name "*.pro")
   do
      sed -i "s/\$\$\[QT_INSTALL_IMPORTS\]/\/usr\/lib\/qt4\/imports/" ${PRO}
   done

}

do_install() {
   oe_runmake INSTALL_ROOT="${D}" install
#   find "${D}" -name "*.pc" | xargs sed -i "/prefix=/d"
#   install -d "${D}/usr/lib/qt4"
#   mv "${D}/usr/plugins" "${D}//usr/lib/qt4"
}

FILES_${PN} = "/usr/lib/qt4/imports/QtDesktop"
FILES_${PN}-dbg = "/usr/lib/qt4/imports/QtDesktop/plugin/.debug/libstyleplugin.so"
