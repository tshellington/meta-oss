#PR = "${INC_PR}.3"
PRINC = "1"
#                    ${QT_SQL_DRIVER_FLAGS} \

QT_CONFIG_FLAGS = "-release -no-cups -reduce-relocations \
                    -shared -no-nas-sound -no-nis \
                    -system-libjpeg -system-libpng -system-libtiff -system-zlib \
                    -no-pch -qdbus -stl -glib -phonon -webkit \
                    -xmlpatterns -no-rpath -qt3support -silent \
                    ${@base_contains('DISTRO_FEATURES', 'pulseaudio', '--enable-pulseaudio', '--disable-pulseaudio', d)} \
                    ${QT_DISTRO_FLAGS} \
                    ${QT_GLFLAGS} \
                    -no-embedded \
                    -xrandr \
                    -x11 \
                   "

