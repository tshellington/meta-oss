DESCRIPTION = "Devon IT Qt preloader application"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

inherit qmake2 qt4x11 update-rc.d

INITSCRIPT_NAME = "qtpreloader"
INITSCRIPT_PARAMS = "start 20 2 3 4 5 ."

QMAKE_PROFILES = "${PN}.pro"

SRC_URI = "file://qtpreloader.pro file://main.cpp file://qtpreloader.init"
S = "${WORKDIR}"

do_install () {
        oe_runmake install INSTALL_ROOT="${D}"
        install -d ${D}/etc/init.d
        install -m 755 ${S}/qtpreloader.init ${D}/etc/init.d/qtpreloader
}
