DESCRIPTION = "Disable broken modes on FX1"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

SRC_URI = "file://randrmodes.conf"
PR = "r1"

do_install () {
    install -d ${D}/etc/devonit
    install -m 755 ${WORKDIR}/randrmodes.conf ${D}/etc/devonit/randrmodes.conf
}