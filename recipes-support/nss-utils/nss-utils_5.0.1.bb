DESCRIPTION = "Mozilla's SSL and TLS tools"
HOMEPAGE = "http://www.mozilla.org/projects/security/pki/nss/"

PR = "r3"

LICENSE = "MPL1.1 GPL LGPL"
LIC_FILES_CHKSUM = "file://LICENSE;md5=dc9b6ecd19a14a54a628edaaf23733bf"

DEPENDS = "sqlite3 nss nspr"

SRC_URI = "\
            ftp://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${PV}/source/firefox-${PV}.source.tar.bz2;name=archive \
            file://build-fix.patch \
            file://no-signlibs.patch \
          "

SRC_URI[archive.md5sum] = "6d1f43e402cec84459a3d7f950bd5192"
SRC_URI[archive.sha256sum] = "7016db35da8f274b1606eba2bde4c5adfb9219ec053e2fcfffac63aefd82ab9f"

S = "${WORKDIR}/mozilla-release"

TD = "${S}/tentative-dist"
TDS = "${S}/tentative-dist-staging"

PARALLEL_MAKE = ""

TARGET_CC_ARCH += "${LDFLAGS}"

do_configure_prepend() {
	cp ${S}/security/coreconf/Linux2.6.mk ${S}/security/coreconf/Linux3.0.mk
	cp ${S}/security/coreconf/Linux2.6.mk ${S}/security/coreconf/Linux3.1.mk
	cp ${S}/security/coreconf/Linux2.6.mk ${S}/security/coreconf/Linux3.2.mk
}

do_compile() {
	oe_runmake -C security/nss \
		build_coreconf \
		build_dbm \
		export libs program \
		MOZILLA_CLIENT=1 \
		BUILD_OPT=1 \
		OS_TARGET=Linux \
		OS_TEST="${TARGET_ARCH}" \
		NSPR_INCLUDE_DIR="${STAGING_INCDIR}/firefox-${PV}/nspr" \
		NSPR_LIB_DIR="${STAGING_LIBDIR}/firefox-${PV}" \
		SQLITE3_INCLUDE_DIR="${STAGING_INCDIR}" \
		OPTIMIZER="${CFLAGS}" \
		NS_USE_GCC=1 \
		NSS_USE_SYSTEM_SQLITE=1 \
		NSS_ENABLE_ECC=1 \
		DEFAULT_COMPILER="${CC}" \
		CC="${CC}" \
		CCC="${CXX}" \
		CXX="${CXX}" \
		RANLIB="${RANLIB}" \
		NATIVE_CC="${BUILD_CC}" \
		NATIVE_FLAGS="${BUILD_CFLAGS}" \
                HOST_CC="${BUILD_CC}" \
                MOZ_NATIVE_NSS=1
}

do_install() {
	oe_runmake -C security/nss \
		install \
		MOZILLA_CLIENT=1 \
		BUILD_OPT=1 \
		OS_TARGET=Linux \
		OS_TEST="${TARGET_ARCH}" \
		NSPR_INCLUDE_DIR="${STAGING_INCDIR}/firefox-${PV}/nspr" \
		NSPR_LIB_DIR="${STAGING_LIBDIR}" \
		NS_USE_GCC=1 \
		NSS_USE_SYSTEM_SQLITE=1 \
		NSS_ENABLE_ECC=1 \
		SOURCE_LIB_DIR="${TD}/${libdir}" \
		SOURCE_BIN_DIR="${TD}/${bindir}"

	install -d ${D}/${bindir}
	for binary in ${TD}/${bindir}/*
	do
		install -m 755 -t ${D}/${bindir} $binary
	done

}
