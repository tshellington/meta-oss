require uim.inc
DEPENDS = "gtk+ anthy intltool-native gettext-native libtool-native jpeg poppler"
inherit native autotools pkgconfig
PR = "r3"
LIC_FILES_CHKSUM = "file://COPYING;md5=59cce023e4281fec4db201904d65a0ab"

EXTRA_OECONF += "--disable-xim --without-qt4 --without-qt4-immodule"
S = "${WORKDIR}/uim-${PV}"

do_configure() {
  olddir=`pwd`
  cd ${S}
  export NOCONFIGURE="no"; ./autogen.sh
  oe_runconf $@
  cd ${olddir}
}

SRC_URI[md5sum] = "5878d2c4d37166258815e035e17842bf"
SRC_URI[sha256sum] = "18c9a29e11f543b560bcf5d678f1d915cd73c1fef99725188b2d910f1c3ff8eb"
