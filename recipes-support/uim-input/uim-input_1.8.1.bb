require uim.inc
DEPENDS = "uim-input-native anthy fontconfig libxft libxt glib-2.0 ncurses"
RDEPENDS = "libuim7"
SECTION_uim-gtk2.0 = "x11/inputmethods"
PR = "r4"
LIC_FILES_CHKSUM = "file://COPYING;md5=59cce023e4281fec4db201904d65a0ab"

SRC_URI += "file://uim-module-manager.patch"
SRC_URI += "file://qt4-gcroots-fix-${PV}.patch"
SRC_URI += "file://uim-init.sh"
SRC_URI += "file://uim-toolbar-qt4.desktop"

inherit qmake2 gettext pkgconfig autotools qt4x11
S = "${WORKDIR}/uim-${PV}"

do_configure() {
  olddir=`pwd`
  cd ${S}
  bbnote "test1 ${CONFIGUREOPTS}"
  
  export _QMAKE="${OE_QMAKE_QMAKE}"
  export _QMAKE4="${OE_QMAKE_QMAKE}"
  NOCONFIGURE="no"; ./autogen.sh
  bbnote "test2 ${CONFIGUREOPTS} ${EXTRA_OECONF}"
  oe_runconf
  
}

do_install_append() {
  install -d ${D}/etc/uim
  install -d ${D}/${sysconfdir}/X11/Xsession.d
  install ${WORKDIR}/uim-init.sh ${D}/${sysconfdir}/X11/Xsession.d/91uim
}

PACKAGES =+ "uim-xim uim-utils uim-skk uim-scm uim-gtk2.0-dbg uim-gtk2.0 uim-qt4-dbg uim-qt4 uim-fep uim-common uim-anthy libuim7 libuim-dev"

LEAD_SONAME = "libuim.so.7"
RDEPENDS_uim-input = "libuim7 libqt3support4 libqtsql4"
RDEPENDS_uim-anthy = "qt4-plugin-codec-jpcodecs glibc-charmap-euc-jp glibc-gconv-euc-jp"
RDEPENDS_uim-gtk2.0 = "uim-scm uim-common"

# this should be in RDEPENDS_uim_anth, but doesn't work yet
#${@bb.data.getVar('PREFERRED_PROVIDER_virtual-japanese-font',d)}

DESCRIPTION_libuim7 = "Simple and flexible input method collection and library"
SECTION_libuim7 = "libs/inputmethods"
FILES_libuim7 = "${libdir}/uim/plugin/libuim-* \
                 ${libdir}/uim/notify/ \
                 ${libdir}/libgcroots.so.* \
                 ${libdir}/libuim-custom.so.* \
                 ${datadir}/locale/ja/LC_MESSAGES/uim.mo \
                 ${datadir}/locale/fr/LC_MESSAGES/uim.mo \
                 ${datadir}/locale/ko/LC_MESSAGES/uim.mo \
                 ${libdir}/libuim.so.* \
                 ${sysconfdir}/X11/Xsession.d"

DESCRIPTION_libuim-dev = "Development files for uim"
SECTION_libuim-dev = "devel/libs"
FILES_libuim-dev = "${libdir}/libuim*.a \
                    ${libdir}/libuim*.la \
                    ${libdir}/libuim*.so \
                    ${includedir}/uim \
                    ${libdir}/pkgconfig/uim.pc"

DESCRIPTION_uim-anthy = "Anthy plugin for uim"
FILES_uim-anthy = "${libdir}/uim/plugin/libuim-anthy* \
                   ${datadir}/uim/anthy*.scm"


DESCRIPTION_uim-fep = "uim Front End Processor"
FILES_uim-fep = "${bindir}/uim-fep*"

DESCRIPTION_uim-scm  = "scm module for uim"
FILES_uim-scm = "${libdir}/libuim-scm.so.0.1.0"

DESCRIPTION_uim-gtk2.0-dbg  = "Debugging files for GTK+2.x immodule for uim"
FILES_uim-gtk2.0-dbg = "${libdir}/gtk-2.0/2.10.0/immodules/.debug/*.so*"

DESCRIPTION_uim-gtk2.0  = "GTK+2.x immodule for uim"
FILES_uim-gtk2.0 = "${libdir}/gtk-2.0 \
                    ${bindir}/uim-toolbar-gtk* \
                    ${bindir}/uim-*-gtk \
                    ${bindir}/uim-input-pad-ja \
                    ${datadir}/uim/helperdata/uim-dict-ui.xml \
                    ${datadir}/applications/uim.desktop \
                    /usr/libexec/uim-candwin-tbl-gtk"

DESCRIPTION_uim-qt4-dbg  = "Debugging files for Qt4 immodule for uim"
FILES_uim-qt4-dbg = "${libdir}/qt4/plugins/inputmethods/.debug/*.so*"

DESCRIPTION_uim-qt4  = "Qt4 immodule for uim"
FILES_uim-qt4 = "${libdir}/qt4 \
                 ${bindir}/uim-*qt4 \
                 /usr/libexec/uim-candwin-tbl-qt4"

DESCRIPTION_uim-skk = "SKK plugin for uim"
FILES_uim-skk = "${libdir}/uim/plugin/libuim-skk.* \
                 ${datadir}/uim/skk*.scm"


DESCRIPTION_uim-utils = "Utilities for uim"
FILES_uim-utils = "${bindir}/uim-sh \
                   ${bindir}/uim-module-manager \
                   ${bindir}/uim-help \
		   ${libexecdir}/uim-helper-server"

DESCRIPTION_uim-xim = "A bridge between uim and XIM"
FILES_uim-xim = "${bindir}/uim-xim \
                 ${libexecdir}/uim-candwin-gtk \
                 ${datadir}/man/man1/uim-xim.1 \
                 ${sysconfdir}/X11/xinit/xinput.d/uim*"

DESCRIPTION_uim-common = "Common files for uim"
FILES_uim-common = "${datadir}/uim/pixmaps/*.png \
                    ${datadir}/uim"

SRC_URI[md5sum] = "5878d2c4d37166258815e035e17842bf"
SRC_URI[sha256sum] = "18c9a29e11f543b560bcf5d678f1d915cd73c1fef99725188b2d910f1c3ff8eb"
