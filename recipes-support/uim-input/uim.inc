DESCRIPTION = "A multilingual user input method library"
HOMEPAGE = "http://uim.freedesktop.org/"
LICENSE = "GPL"



SECTION = "inputmethods"

EXTRA_OECONF += "--disable-applet \
                 --disable-emacs \
                 --without-scim \
                 --without-prime \
                 --without-mana \
                 --without-eb \
                 --with-anthy \
                 --with-anthy-utf8 \
                 --with-canna \
                 --enable-dict \
                 --without-libedit \
                 --with-qt4 \
"

SRC_URI = "http://uim.googlecode.com/files/uim-${PV}.tar.bz2"

FILES_${PN}-dbg += "${libdir}/*/*/*/.debug ${libdir}/*/*/.debug"
FILES_${PN}-dev += "${libdir}/uim/plugin/*.la"
FILES_${PN}-dev += "${libdir}/libuim-scm.so"