require uim.inc
DEPENDS = "gtk+ anthy intltool-native gettext-native"
inherit native autotools pkgconfig
PR = "r2"
LIC_FILES_CHKSUM = "file://COPYING;md5=59cce023e4281fec4db201904d65a0ab"

EXTRA_OECONF += "--disable-xim --without-qt4 --without-qt4-immodule"
S = "${WORKDIR}/uim-${PV}"

do_configure() {
  olddir=`pwd`
  cd ${S}
  export NOCONFIGURE="no"; ./autogen.sh
  oe_runconf $@
  cd ${olddir}
}

SRC_URI[md5sum] = "80027d3706f28d1dff9a159139b87adf"
SRC_URI[sha256sum] = "81964ed6786eaa8306b0a638193db8171b78b386b9443d2e6a78e7f2cdf9a773"
