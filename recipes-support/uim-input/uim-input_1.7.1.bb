require uim.inc
DEPENDS = "uim-input-native anthy fontconfig libxft libxt glib-2.0 ncurses"
RDEPENDS = "libuim7"
SECTION_uim-gtk2.0 = "x11/inputmethods"
PR = "r4"
LIC_FILES_CHKSUM = "file://COPYING;md5=59cce023e4281fec4db201904d65a0ab"

SRC_URI += "file://uim-module-manager.patch"
SRC_URI += "file://qt4-gcroots-fix-${PV}.patch"
SRC_URI += "file://qt4-toolbar-embed-${PV}.patch"
SRC_URI += "file://uim-init.sh"
SRC_URI += "file://uim-toolbar-qt4.desktop"

inherit qmake2 autotools pkgconfig
S = "${WORKDIR}/uim-${PV}"

do_configure() {
  olddir=`pwd`
  cd ${S}
  export NOCONFIGURE="no"
  export _QMAKE="${OE_QMAKE_QMAKE}"
  if [ -e configure ]; then
    rm configure
  fi
  ./autogen.sh
  oe_runconf
  cd ${olddir}
}

do_install_append() {
  install -d ${D}/etc/uim
  install -d ${D}/${sysconfdir}/X11/Xsession.d
  install ${WORKDIR}/uim-init.sh ${D}/${sysconfdir}/X11/Xsession.d/91uim
}

PACKAGES =+ "uim-xim uim-utils uim-skk uim-scm uim-gtk2.0-dbg uim-gtk2.0 uim-qt4-dbg uim-qt4 uim-fep uim-common uim-anthy libuim7 libuim-dev"

LEAD_SONAME = "libuim.so.7"
RDEPENDS_uim-input = "libuim7 libqt3support4 libqtsql4"
RDEPENDS_uim-anthy = "qt4-plugin-codec-jpcodecs glibc-charmap-euc-jp glibc-gconv-euc-jp"
RDEPENDS_uim-gtk2.0 = "uim-scm uim-common"

# this should be in RDEPENDS_uim_anth, but doesn't exist ${@bb.data.getVar('PREFERRED_PROVIDER_virtual-japanese-font',d)}

DESCRIPTION_libuim7 = "Simple and flexible input method collection and library"
SECTION_libuim7 = "libs/inputmethods"
FILES_libuim7 = "${libdir}/uim/plugin/libuim-* \
                 ${libdir}/uim/notify/ \
                 ${libdir}/libgcroots.so.* \
                 ${libdir}/libuim-custom.so.* \
                 ${datadir}/locale/ja/LC_MESSAGES/uim.mo \
                 ${datadir}/locale/fr/LC_MESSAGES/uim.mo \
                 ${datadir}/locale/ko/LC_MESSAGES/uim.mo \
                 ${libdir}/libuim.so.* \
                 ${sysconfdir}/X11/Xsession.d"

pkg_postinst_uim-input() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    # Only enable the modules we want, and disable the ones we don't that are enabled by default
    uim-module-manager --register anthy --path $D/usr/share/uim
    uim-module-manager --unregister anthy-utf8 --path $D/usr/share/uim
    uim-module-manager --unregister skk --path $D/usr/share/uim
    uim-module-manager --unregister tcode --path $D/usr/share/uim
    uim-module-manager --unregister trycode --path $D/usr/share/uim
    uim-module-manager --unregister tutcode --path $D/usr/share/uim
    uim-module-manager --unregister byeoru --path $D/usr/share/uim
    uim-module-manager --unregister latin --path $D/usr/share/uim
    uim-module-manager --unregister elatin --path $D/usr/share/uim
    uim-module-manager --register pyload --path $D/usr/share/uim
    uim-module-manager --register hangul --path $D/usr/share/uim
    uim-module-manager --unregister viqr --path $D/usr/share/uim
    uim-module-manager --unregister ipa-x-sampa --path $D/usr/share/uim
    uim-module-manager --unregister look --path $D/usr/share/uim
    sed -i "s/.define installed-im-list .*/(define installed-im-list '(anthy py pinyin-big5 romaja))/" $D/usr/share/uim/installed-modules.scm
    sed -i "s/define-custom 'toolbar-show-pref-button? #t/define-custom 'toolbar-show-pref-button? #f/" $D/usr/share/uim/im-custom.scm
    sed -i "s/define-custom 'toolbar-show-action-based-switcher-button? #t/define-custom 'toolbar-show-action-based-switcher-button? #f/" $D/usr/share/uim/im-custom.scm
fi
}

DESCRIPTION_libuim-dev = "Development files for uim"
SECTION_libuim-dev = "devel/libs"
FILES_libuim-dev = "${libdir}/libuim*.a \
                    ${libdir}/libuim*.la \
                    ${libdir}/libuim*.so \
                    ${includedir}/uim \
                    ${libdir}/pkgconfig/uim.pc"

DESCRIPTION_uim-anthy = "Anthy plugin for uim"
FILES_uim-anthy = "${libdir}/uim/plugin/libuim-anthy* \
                   ${datadir}/uim/anthy*.scm"

pkg_postinst_uim-anthy() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --register anthy --path $D/usr/share/uim
fi
}

pkg_postrm_uim-anthy() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --path $D/usr/share/uim --unregister anthy
fi
}

pkg_prerm_uim-anthy() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --register anthy --path $D/usr/share/uim
fi
}

DESCRIPTION_uim-fep = "uim Front End Processor"
FILES_uim-fep = "${bindir}/uim-fep*"

DESCRIPTION_uim-scm  = "scm module for uim"
FILES_uim-scm = "${libdir}/libuim-scm.so*"

DESCRIPTION_uim-gtk2.0-dbg  = "Debugging files for GTK+2.x immodule for uim"
FILES_uim-gtk2.0-dbg = "${libdir}/gtk-2.0/2.10.0/immodules/.debug/*.so*"

DESCRIPTION_uim-gtk2.0  = "GTK+2.x immodule for uim"
FILES_uim-gtk2.0 = "${libdir}/gtk-2.0 \
                    ${bindir}/uim-toolbar-gtk* \
                    ${bindir}/uim-*-gtk \
                    ${bindir}/uim-input-pad-ja \
                    ${datadir}/uim/helperdata/uim-dict-ui.xml \
                    ${datadir}/applications/uim.desktop \
                    /usr/libexec/uim-candwin-tbl-gtk"

DESCRIPTION_uim-qt4-dbg  = "Debugging files for Qt4 immodule for uim"
FILES_uim-qt4-dbg = "${libdir}/qt4/plugins/inputmethods/.debug/*.so*"

DESCRIPTION_uim-qt4  = "Qt4 immodule for uim"
FILES_uim-qt4 = "${libdir}/qt4 \
                 ${bindir}/uim-*qt4 \
                 /usr/libexec/uim-candwin-tbl-qt4"

DESCRIPTION_uim-skk = "SKK plugin for uim"
FILES_uim-skk = "${libdir}/uim/plugin/libuim-skk.* \
                 ${datadir}/uim/skk*.scm"

pkg_postinst_uim-skk() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --register skk --path $D/usr/share/uim
fi
}

pkg_prerm_uim-skk() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --register skk --path $D/usr/share/uim
fi
}

pkg_postrm_uim-skk() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --path $D/usr/share/uim --unregister skk
fi
}

DESCRIPTION_uim-utils = "Utilities for uim"
FILES_uim-utils = "${bindir}/uim-sh \
                   ${bindir}/uim-module-manager \
                   ${bindir}/uim-help \
		   ${libexecdir}/uim-helper-server"

DESCRIPTION_uim-xim = "A bridge between uim and XIM"
FILES_uim-xim = "${bindir}/uim-xim \
                 ${libexecdir}/uim-candwin-gtk \
                 ${datadir}/man/man1/uim-xim.1 \
                 ${sysconfdir}/X11/xinit/xinput.d/uim*"

DESCRIPTION_uim-common = "Common files for uim"
FILES_uim-common = "${datadir}/uim/pixmaps/*.png \
                    ${datadir}/uim"
pkg_postinst_uim-common() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --path $D/usr/share/uim --register hangul
fi
}

pkg_prerm_uim-common() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --path $D/usr/share/uim --register hangul
}

pkg_prerm_uim-common() {
#!/bin/sh
set -e
if which uim-module-manager >/dev/null 2>&1; then
    uim-module-manager --path $D/usr/share/uim --unregister hangul
fi
}

SRC_URI[md5sum] = "80027d3706f28d1dff9a159139b87adf"
SRC_URI[sha256sum] = "81964ed6786eaa8306b0a638193db8171b78b386b9443d2e6a78e7f2cdf9a773"
