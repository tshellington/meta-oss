
#run update-alternatives when building image
#need to test this for arm
pkg_postinst_${PN} () {
    for app in add-shell  installkernel  mkboot  remove-shell ; do
        update-alternatives --install ${sbindir}/$app $app $app.${PN} 100
    done

    for app in savelog  sensible-browser  sensible-editor  sensible-pager  which ; do
        update-alternatives --install ${bindir}/$app $app $app.${PN} 100
    done

    for app in run-parts  tempfile ; do
        update-alternatives --install ${base_bindir}/$app $app $app.${PN} 100
    done
}

