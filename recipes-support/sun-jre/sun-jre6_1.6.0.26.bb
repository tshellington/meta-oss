
DESCRIPTION = "Sun J2SE Runtime Environment"

PR="r1"

LICENSE = "Sun Binary Code License Agreement"
LIC_FILES_CHKSUM = "file://jre1.6.0_26/LICENSE;md5=98f46ab6481d87c4d77e0e91a6dbc15f"
SP = "sun-java6_6-26.orig.tar.gz"
IP = "jdk-6u26-dlj-linux-i586.bin"
SD = "jre1.6.0_26"

SRC_URI = "http://javadl.sun.com/webapps/download/AutoDL?BundleId=49015"

SRC_URI[md5sum] = "9a8718922965deedd662439fdc3bc467"
SRC_URI[sha256sum] = "fe288ffec75b52392f37bf367134fc4bd66a6e4efd9958da2c7f1e7118931947"

S = "${WORKDIR}"

do_configure() {
    cp ${WORKDIR}/AutoDL?BundleId=49015 ${WORKDIR}/${IP}
    sh ${IP} --accept-license --unpack
}

do_install() {

    install -d ${D}/opt/java/bin
    install -d ${D}/opt/java/lib
    install -d ${D}/usr/bin
    install -d ${D}/etc/ld.so.conf.d
    install -d ${D}/usr/lib/mozilla/plugins

    echo "/opt/java/lib/i386" > ${D}/etc/ld.so.conf.d/java

    pushd ${SD}
       cp -r bin ${D}/opt/java
       cp -r lib ${D}/opt/java
       cp    plugin/i386/ns7/libjavaplugin_oji.so ${D}/usr/lib/mozilla/plugins/libjavaplugin.so
       #ln -s /usr/bin/java ${D}/opt/java/bin/java
       #ln -s /usr/bin/javaws ${D}/opt/java/bin/javaws
    popd

}

do_package_qa() {
}

PACKAGES =+ "${PN}-plugin"

FILES_${PN} = "/opt /usr/bin /etc"
FILES_${PN}-plugin = "/usr/lib/mozilla/plugins/libjavaplugin.so"

RDEPENDS_${PN} += "libx11-locale"
RDEPENDS_${PN}-plugin += "${PN}"

COMPATIBLE_HOST = "i.86.*-linux"
