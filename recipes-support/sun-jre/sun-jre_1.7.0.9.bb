
DESCRIPTION = "Sun J2SE Runtime Environment"

PR="r5"

LICENSE = "Sun_Binary_Code_License_Agreement"
LIC_FILES_CHKSUM = "file://jre1.7.0_09/COPYRIGHT;md5=288a0f8d9b06544178435b0efa752c08"
IP = "jre-7u9-linux-i586.tar.gz"
SD = "jre1.7.0_09"

SRC_URI = "http://javadl.sun.com/webapps/download/AutoDL?BundleId=69465"
SRC_URI[md5sum] = "56178ed00dab2ebd8268caf5575743f4"
SRC_URI[sha256sum] = "37310199e770f10d8e5f9410757062655b66723ca2b8c4dd29d9a4e9bd7d98bd"

do_configure() {
    cp ${WORKDIR}/AutoDL?BundleId=69465 ${WORKDIR}/${IP}
    tar -xf ${WORKDIR}/${IP}
}

do_install() {

    install -d ${D}/opt/java
    install -d ${D}/usr/bin
    install -d ${D}/etc/ld.so.conf.d
    install -d ${D}/usr/lib/mozilla/plugins

    echo "/opt/java/lib/i386" > ${D}/etc/ld.so.conf.d/java
 
    pushd ${SD}
       cp -r bin ${D}/opt/java
       cp -r lib ${D}/opt/java
       ln -s /opt/java/lib/i386/libnpjp2.so ${D}/usr/lib/mozilla/plugins/libjavaplugin.so
    popd

}

do_package_qa() {
}

PACKAGES =+ "${PN}-plugin"

FILES_${PN} = "/opt /usr/bin /etc"
FILES_${PN}-dbg = "/opt/java/lib/i386/headless/.debug \
                   /opt/java/lib/i386/jli/.debug \
                   /opt/java/lib/i386/client/.debug \
                   /opt/java/lib/i386/server/.debug \
                   /opt/java/lib/i386/xawt/.debug \
                   /opt/java/lib/i386/.debug \
                   /opt/java/lib/.debug \
                   /opt/java/bin/.debug \
                  "

FILES_${PN}-plugin = "/usr/lib/mozilla/plugins/libjavaplugin.so"

RDEPENDS_${PN} += "libx11-locale"
RDEPENDS_${PN}-plugin += "${PN}"

COMPATIBLE_HOST = "i.86.*-linux"
