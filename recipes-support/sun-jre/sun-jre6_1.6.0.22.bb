
DESCRIPTION = "Sun J2SE Runtime Environment"

PR="r1"

LICENSE = "Sun Binary Code License Agreement"

SP = "sun-java6_6-22.orig.tar.gz"
IP = "jdk-6u22-dlj-linux-i586.bin"
SD = "jdk1.6.0_22"

SRC_URI = "http://ftp.de.debian.org/debian/pool/non-free/s/sun-java6/${SP}"

SRC_URI[md5sum] = "981bd97edf98849f108df9d3d40352bb"
SRC_URI[sha256sum] = "6c144a6524cb811ab4fa67ea857474d231c77222088166660b3957ed6dc1678c"

S = "${WORKDIR}/sun-java6-6.22"

do_configure() {
    sh ${IP} --accept-license --unpack
}

do_install() {

    install -d ${D}/opt/java/bin
    install -d ${D}/opt/java/lib
    install -d ${D}/usr/bin
    install -d ${D}/etc/ld.so.conf.d
    install -d ${D}/usr/lib/mozilla/plugins

    echo "/opt/java/lib/i386" > ${D}/etc/ld.so.conf.d/java

    pushd ${SD}
       cp -r jre/bin ${D}/opt/java/bin
       cp -r jre/lib ${D}/opt/java/lib
       cp jre/plugin/i386/ns7/libjavaplugin_oji.so ${D}/usr/lib/mozilla/plugins/libjavaplugin.so
       ln -s /usr/bin/java ${D}/opt/java/bin/java
       ln -s /usr/bin/javaws ${D}/opt/java/bin/javaws
    popd

}

do_package_qa() {
}

PACKAGES =+ "${PN}-plugin"

FILES_${PN} = "/opt /usr/bin /etc"
FILES_${PN}-plugin = "/usr/lib/mozilla/plugins/libjavaplugin.so"

RDEPENDS_${PN} += "libx11-locale"
RDEPENDS_${PN}-plugin += "${PN}"

COMPATIBLE_HOST = "i.86.*-linux"
