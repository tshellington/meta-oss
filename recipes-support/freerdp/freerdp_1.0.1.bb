# Copyright (C) 2010, 2011 O.S. Systems Software Ltda. All Rights Reserved
# Released under the MIT license

DESCRIPTION = "FreeRDP RDP client"
HOMEPAGE = "http://www.freerdp.com"
DEPENDS = "virtual/libx11 openssl libxcursor libxv cups alsa-lib pulseaudio pcsc-lite ffmpeg docbook-sgml-dtd-4.5-native"
SECTION = "x11/network"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

inherit pkgconfig cmake

PR = "r11"

SRCREV = "${PV}"
SRC_URI = " \
            git://github.com/FreeRDP/FreeRDP.git;protocol=git \
            file://pulseaudio.patch \
          "

S = "${WORKDIR}/git"

EXTRA_OECMAKE += "-DWITH_PCSC=ON -DWITH_MANPAGES=OFF -DWITH_PULSEAUDIO=ON"

PACKAGES =+ "libfreerdp"

LEAD_SONAME = "libfreerdp.so"
FILES_libfreerdp = "${libdir}/lib*${SOLIBS}"

PACKAGES_DYNAMIC = "libfreerdp-plugin-*"

PULSE_RDEPENDS += "libpulse \
                  libpulsecore \
                  libpulse-mainloop-glib \
                  libpulse-simple \
                 "

RDEPENDS_libfreerdp-plugin-audin-pulse += "${PULSE_RDEPENDS}"
RDEPENDS_libfreerdp-plugin-rdpsnd-pulse += "${PULSE_RDEPENDS}"
RDEPENDS_libfreerdp-plugin-tsmf-pulse += "${PULSE_RDEPENDS}"

python populate_packages_prepend () {
        freerdp_root = bb.data.expand('${libdir}/freerdp', d)

        do_split_packages(d, freerdp_root, '^(audin_.*)\.so$',
                output_pattern='libfreerdp-plugin-%s',
                description='FreeRDP plugin %s',
                prepend=True, extra_depends='libfreerdp-plugin-audin')

        do_split_packages(d, freerdp_root, '^(rdpsnd_.*)\.so$',
                output_pattern='libfreerdp-plugin-%s',
                description='FreeRDP plugin %s',
                prepend=True, extra_depends='libfreerdp-plugin-rdpsnd')

        do_split_packages(d, freerdp_root, '^(tsmf_.*)\.so$',
                output_pattern='libfreerdp-plugin-%s',
                description='FreeRDP plugin %s',
                prepend=True, extra_depends='libfreerdp-plugin-tsmf')

        do_split_packages(d, freerdp_root, '^([^-]*)\.so$',
                output_pattern='libfreerdp-plugin-%s',
                description='FreeRDP plugin %s',
                prepend=True, extra_depends='')
}

